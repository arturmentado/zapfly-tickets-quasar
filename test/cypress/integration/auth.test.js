let admin;
let att;

describe('Create account and attendant', () => {
  before(() => {
    cy.resetDatabase();

    cy.fixture('admin')
      .then((e) => {
        admin = e;
      });

    cy.fixture('attendant')
      .then((e) => {
        att = e;
      });
  });

  it('should validate registration', () => {
    cy.visit('/');
    cy.contains('button', 'Criar registro').click();

    cy.get('.q-field > .q-field__inner > .q-field__control > .q-field__control-container > [aria-label="Nome"]').type(admin.name);
    cy.get('.q-field > .q-field__inner > .q-field__control > .q-field__control-container > [aria-label="Ultimo nome"]').type(admin.lastName);
    cy.get('.q-field > .q-field__inner > .q-field__control > .q-field__control-container > [aria-label="E-mail"]').type(admin.email);

    cy.get('.q-field > .q-field__inner > .q-field__control > .q-field__control-container > [aria-label="Senha"]').type(admin.password);
    cy.get('.q-field > .q-field__inner > .q-field__control > .q-field__control-container > [aria-label="Confirme a senha"]').type(admin.password);
    cy.get('.q-field > .q-field__inner > .q-field__control > .q-field__control-container > [aria-label="Numero de Whatsapp"]').type(admin.phone);

    cy.get('button').contains('Criar conta').click();
    cy.get('body').contains('Você precisa aceitar os termos de uso para usar a plataforma');
  });

  it('should register a new user', () => {
    cy.get(':nth-child(7) > .q-checkbox > .q-checkbox__inner').click();
    cy.get(':nth-child(8) > .q-checkbox > .q-checkbox__inner').click();
    cy.get('button').contains('Criar conta').click();

    cy.get('body').contains('Novos atendimentos');
  });

  it('should be able to logout', () => {
    cy.get('.q-drawer__content .material-icons').contains('exit_to_app').click();
    cy.location('href').should('eq', `${Cypress.config().baseUrl}`);
  });

  // it('should be able to login again', () => {
  //   cy.get('.q-field > .q-field__inner > .q-field__control > .q-field__control-container > [aria-label="Email"]').type(admin.email);
  //   cy.get('.q-field > .q-field__inner > .q-field__control > .q-field__control-container > [aria-label="Senha"]').type(admin.password);

  //   cy.get('.full-width > .q-card__actions > .q-btn:nth-child(1) > .q-btn__wrapper > .q-btn__content').click();
  //   cy.get('body').contains('O acesso está liberado. Liberado até ');
  //   cy.wait(2000);
  // });

  // it('should create a attendant', () => {
  //   cy.visit('/acessar');
  //   cy.get('.q-field > .q-field__inner > .q-field__control > .q-field__control-container > [aria-label="Email"]').type(admin.email);
  //   cy.get('.q-field > .q-field__inner > .q-field__control > .q-field__control-container > [aria-label="Senha"]').type(admin.password);
  //   cy.get('.full-width > .q-card__actions > .q-btn:nth-child(1) > .q-btn__wrapper > .q-btn__content').click();

  //   cy.contains('div.text-h6.flex', 'Atendentes')
  //     .contains('button', 'add')
  //     .click();

  //   // cy.get('body > div.q-layout.q-layout--standard > div.q-page-container > div > div > div.col-xs-12.col-md-6 > div > div:nth-child(1) > div > button > div.q-btn__wrapper.col.row.q-anchor--skip > div > i').click()

  //   cy.get('.q-field > .q-field__inner > .q-field__control > .q-field__control-container > [aria-label="Nome"]').type(att.name);
  //   cy.get('.q-field > .q-field__inner > .q-field__control > .q-field__control-container > [aria-label="E-mail"]').type(att.email);
  //   // cy.get('.q-field__control > .q-field__control-container > .q-field__native > .q-chip > .q-chip__icon').click()
  //   cy.get('.q-field > .q-field__inner > .q-field__control > .q-field__control-container > [aria-label="Senha"]').type(att.password);
  //   cy.get('.q-field > .q-field__inner > .q-field__control > .q-field__control-container > [aria-label="Confirmar Senha"]').type(att.password);

  //   cy.get('.q-card__actions > .q-btn:nth-child(2)').click();
  //   cy.get('body').contains('atendente 99');
  // });

  // it('should attendants be able to login', () => {
  //   cy.visit('/acessar');
  //   cy.get('.q-field > .q-field__inner > .q-field__control > .q-field__control-container > [aria-label="Email"]').type(att.email);
  //   cy.get('.q-field > .q-field__inner > .q-field__control > .q-field__control-container > [aria-label="Senha"]').type(att.password);
  //   cy.get('.full-width > .q-card__actions > .q-btn:nth-child(1) > .q-btn__wrapper > .q-btn__content').click();

  //   cy.get('.q-drawer__content > .q-list > .q-item:nth-child(4) > .q-item__section > .material-icons').click();

  //   cy.get('.q-field > .q-field__inner > .q-field__control > .q-field__control-container > [aria-label="Email"]').type(att.email);
  //   cy.get('.q-field > .q-field__inner > .q-field__control > .q-field__control-container > [aria-label="Senha"]').type(att.password);

  //   cy.get('.full-width > .q-card__actions > .q-btn:nth-child(1) > .q-btn__wrapper > .q-btn__content').click();
  //   cy.get('body').contains('O acesso está liberado. Liberado até ');
  //   cy.contains('.q-card', 'Atendentes').should('not.be.visible');
  // });
});
