let admin;

describe('Admin Users', () => {
  before(() => {
    cy.resetDatabase().then(() => {
      cy.createAdmin(true);
    });

    cy.fixture('admin')
      .then((e) => {
        admin = e;
      });
  });

  it('should see admin menu', () => {
    cy.visit('/');
    cy.get('[aria-label="E-mail"]').type(admin.email);
    cy.get('.q-field > .q-field__inner > .q-field__control > .q-field__control-container > [aria-label="Senha"]').type(admin.password);
    cy.get('.full-width > .q-card__actions > .q-btn:nth-child(1) > .q-btn__wrapper > .q-btn__content').click();
    cy.get('.q-expansion-item > .q-expansion-item__container > .q-item > .q-item__section > .fas').click();

    cy.contains('.q-item', 'Usuarios')
      .click();

    cy.contains('.q-page-container > .row > .col-lg-12 > .q-table__container > .q-table__bottom', '1-1 of 1');
  });
  it('should collapse searches in mobile', () => {
    cy.viewport(375, 500);
    cy.get('input[aria-label="Busca"]').should('not.be.visible');
    cy.contains('.q-btn', 'more').click();
    cy.get('input[aria-label="Busca"]').should('be.visible');
  });
});
