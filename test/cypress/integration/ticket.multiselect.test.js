let admin;

describe('Tickets multiselect', () => {
  before(() => {
    cy.resetDatabase().then(() => {
      cy.createAdmin(true);
    });

    cy.fixture('admin')
      .then((e) => {
        admin = e;
      });
  });

  it('should open ticket room', () => {
    cy.visit('/');
    cy.get('.q-field > .q-field__inner > .q-field__control > .q-field__control-container > [aria-label="E-mail"]').type(admin.email);
    cy.get('.q-field > .q-field__inner > .q-field__control > .q-field__control-container > [aria-label="Senha"]').type(admin.password);
    cy.get('.full-width > .q-card__actions > .q-btn:nth-child(1) > .q-btn__wrapper > .q-btn__content').click();

    cy.sendState(admin.phone, 'open');
    cy.sendMessage(admin.phone);

    cy.get('[href="/dashboard/a"]').first().click();
  });

  it('should enable multiselect mode', () => {
    cy.get('.sidemenu-scroll .q-item .ticket-avatar');
    cy.get('.q-item__label > .q-field > .q-field__inner > .q-field__control > .q-field__append > .q-btn > .q-btn__wrapper > .q-btn__content > .material-icons').click();
    cy.contains('.q-item', 'Selecionar varios').click();
    cy.contains('.sidemenu-scroll .q-item .multi-select', 'check_box_outline_blank').click();
    cy.contains('.sidemenu-scroll .q-item .multi-select', 'check_box').click();
    cy.get('.q-item__label > .q-field > .q-field__inner > .q-field__control > .q-field__append > .q-btn > .q-btn__wrapper > .q-btn__content > .material-icons').click();
    cy.contains('.q-item', 'Selecionar varios').click();
    cy.get('.sidemenu-scroll .q-item .ticket-avatar');
  });

  it('select at least one in multiselect should allow to close them all', () => {
    cy.get('.sidemenu-scroll .q-item .ticket-avatar');
    cy.get('.q-item__label > .q-field > .q-field__inner > .q-field__control > .q-field__append > .q-btn > .q-btn__wrapper > .q-btn__content > .material-icons').click();
    cy.contains('.q-item', 'Selecionar varios').click();
    cy.contains('.sidemenu-scroll .q-item .multi-select', 'check_box_outline_blank').click();
    cy.get('[icon="close"] > .q-btn__wrapper > .q-btn__content > .material-icons').click();
    cy.contains('.q-dialog', 'Tem certeza que deseja fechar 1 atendimentos?');
    cy.contains('.q-dialog .q-btn', 'Sim').click();
    cy.get('.sidemenu-scroll .q-item').should('not.be.exist');
  });
});
