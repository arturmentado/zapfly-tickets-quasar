let admin; let
  att;

describe('Admin can block access of connections to FE', () => {
  before(() => {
    cy.resetDatabase().then(() => {
      cy.createAdmin(true);
    });

    cy.fixture('admin')
      .then((e) => {
        admin = e;
      });

    cy.fixture('attendant')
      .then((e) => {
        att = e;
      });
  });

  it('should login', () => {
    cy.visit('/');
    cy.get('[aria-label="E-mail"]').type(admin.email);
    cy.get('.q-field > .q-field__inner > .q-field__control > .q-field__control-container > [aria-label="Senha"]').type(admin.password);
    cy.get('.full-width > .q-card__actions > .q-btn:nth-child(1) > .q-btn__wrapper > .q-btn__content').click();
  });

  it('should create a attendant', () => {
    cy.contains('div.text-h6.flex', 'Atendentes')
      .contains('button', 'add')
      .click();

    cy.get('.q-field > .q-field__inner > .q-field__control > .q-field__control-container > [aria-label="Nome"]').type(att.name);
    cy.get('.q-dialog [aria-label="Ultimo nome"]').type(att.lastName);
    cy.get('.q-field > .q-field__inner > .q-field__control > .q-field__control-container > [aria-label="E-mail"]').type(att.email);
    cy.get('.q-field > .q-field__inner > .q-field__control > .q-field__control-container > [aria-label="Senha"]').type(att.password);
    cy.get('.q-dialog [aria-label="Confirme a senha"]').type(att.password);

    cy.get('.q-card__actions > .q-btn:nth-child(2)').click();
    cy.get('body').contains('atendente 99');
  });

  it('should block access', () => {
    cy.get('.q-expansion-item > .q-expansion-item__container > .q-item > .q-item__section > .fas').click();
    cy.contains('.q-item', 'Conexões')
      .click();
    cy.contains('.q-btn', 'block')
      .click();
    cy.contains('.q-item', 'Atendendo pedido de suporte')
      .click();
    cy.contains('Sucesso');
  });

  it('should be blocked with other account', () => {
    cy.visit('/');
    cy.get('[aria-label="E-mail"]').type(att.email);
    cy.get('.q-field > .q-field__inner > .q-field__control > .q-field__control-container > [aria-label="Senha"]').type(att.password);
    cy.get('.full-width > .q-card__actions > .q-btn:nth-child(1) > .q-btn__wrapper > .q-btn__content').click();
    cy.contains('Atendendo pedido de suporte');
  });

  it('should unblock access', () => {
    cy.visit('/');
    cy.get('[aria-label="E-mail"]').type(admin.email);
    cy.get('.q-field > .q-field__inner > .q-field__control > .q-field__control-container > [aria-label="Senha"]').type(admin.password);
    cy.get('.full-width > .q-card__actions > .q-btn:nth-child(1) > .q-btn__wrapper > .q-btn__content').click();

    cy.get('.q-expansion-item > .q-expansion-item__container > .q-item > .q-item__section > .fas').click();
    cy.contains('.q-item', 'Conexões')
      .click();
    cy.contains('.q-btn', 'block')
      .click();
    cy.contains('.q-item', '- Remover bloqueio -')
      .click();
    cy.contains('Sucesso');
  });

  it('should not be blocked with other account', () => {
    cy.visit('/');
    cy.get('[aria-label="E-mail"]').type(att.email);
    cy.get('.q-field > .q-field__inner > .q-field__control > .q-field__control-container > [aria-label="Senha"]').type(att.password);
    cy.get('.full-width > .q-card__actions > .q-btn:nth-child(1) > .q-btn__wrapper > .q-btn__content').click();
    cy.contains('ZapFly Atd');
  });
});
