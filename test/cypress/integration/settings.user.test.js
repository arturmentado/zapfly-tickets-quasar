describe('Personal settings', () => {
  before(() => {
    cy.resetDatabase().then(() => {
      cy.createAdmin();
    });
  });

  it('should enable/disable the sound for notifications', () => {
    cy.visit('/');
    cy.get('.q-field > .q-field__inner > .q-field__control > .q-field__control-container > [aria-label="E-mail"]').type('teste@teste.com');
    cy.get('.q-field > .q-field__inner > .q-field__control > .q-field__control-container > [aria-label="Senha"]').type('teste');
    cy.get('.full-width > .q-card__actions > .q-btn:nth-child(1) > .q-btn__wrapper > .q-btn__content').click();

    cy.get('#top_controls > .q-btn:nth-child(1) > .q-btn__wrapper > .q-btn__content > .material-icons').contains('volume_off');
    cy.get('#top_controls > .q-btn:nth-child(1) > .q-btn__wrapper > .q-btn__content > .material-icons').click();

    cy.get('#top_controls > .q-btn:nth-child(1) > .q-btn__wrapper > .q-btn__content > .material-icons').contains('volume_down');
    cy.get('#top_controls > .q-btn:nth-child(1) > .q-btn__wrapper > .q-btn__content > .material-icons').click();

    cy.get('#top_controls > .q-btn:nth-child(1) > .q-btn__wrapper > .q-btn__content > .material-icons').contains('volume_off');
  });

  it('should use signature settings', () => {
    cy.get('a[href="/dashboard/s"]').click();
    cy.contains('button', 'Configurações de usuario').click();

    cy.get(' .q-toggle').contains('Assinar minhas mensagens').click();
    cy.get('.q-message').contains('Teste Admin Esta é uma mensagem de exemplo!');

    cy.get('.q-toggle').contains('Assinar minhas mensagens no final').click();
    cy.get('.q-message').contains('Esta é uma mensagem de exemplo! Teste Admin ');

    cy.get('.q-toggle').contains('Assinar minhas mensagens no final').click();
    cy.get('.q-message').contains('Teste Admin Esta é uma mensagem de exemplo!');

    cy.get('input[aria-label="Personalizar assinatura"]').clear().type('*<meu-nome>22*');
    cy.get('.q-message').contains('Teste Admin22 Esta é uma mensagem de exemplo!');

    cy.get('input[aria-label="Personalizar assinatura"]').clear().type('*<meu-nome>*');
    cy.get('.q-message').contains('Teste Admin Esta é uma mensagem de exemplo!');

    cy.get('.q-toggle').contains('Assinar minhas mensagens').click();
    cy.contains('.q-dialog button', 'Enviar').click();
  });

  it('should change personal information', () => {
    cy.contains('button', 'Atualizar dados da Conta').click();
    cy.get('input[aria-label="Nome"]').clear().type('Testado');
    cy.get('input[aria-label="E-mail"]').clear().type('testado@teste.com');
    cy.contains('.q-dialog button', 'Enviar').click();

    cy.contains('.q-item', 'exit_to_app').click();
    cy.visit('/');

    cy.get('.q-field > .q-field__inner > .q-field__control > .q-field__control-container > [aria-label="E-mail"]').type('testado@teste.com');
    cy.get('.q-field > .q-field__inner > .q-field__control > .q-field__control-container > [aria-label="Senha"]').type('teste');

    cy.get('.full-width > .q-card__actions > .q-btn:nth-child(1) > .q-btn__wrapper > .q-btn__content').click();

    cy.get('a[href="/dashboard/s"]').click();
    cy.contains('button', 'Atualizar dados da Conta').click();
    cy.get('input[aria-label="Nome"]').clear().type('Teste');
    cy.get('input[aria-label="E-mail"]').clear().type('teste@teste.com');
    cy.contains('.q-dialog button', 'Enviar').click();
    cy.wait(200);
  });

  it('should change language', () => {
    cy.contains('button', 'Atualizar dados da Conta').click();
    cy.get('[label="Linguagem"').click();
    cy.contains('.q-item', 'English').click();
    cy.contains('.q-dialog button', 'Enviar').click();
    cy.contains('button', 'Update account data').click();
    cy.get('[label="Language"]').click();
    cy.contains('.q-item', 'Português').click();
    cy.contains('.q-dialog button', 'Send').click();
    cy.contains('button', 'Atualizar dados da Conta');
  });

  it('should change password', () => {
    cy.contains('button', 'Atualizar minha senha').click();

    cy.get('.q-field > .q-field__inner > .q-field__control > .q-field__control-container > [aria-label="Nova Senha"]').type('t');
    cy.get('.q-field > .q-field__inner > .q-field__control > .q-field__control-container > [aria-label="Confirme a nova senha"]').type('t');
    cy.contains('.q-dialog button', 'Enviar').click();

    cy.contains('.q-item', 'exit_to_app').click();
    cy.visit('/');

    cy.get('.q-field > .q-field__inner > .q-field__control > .q-field__control-container > [aria-label="E-mail"]').type('teste@teste.com');
    cy.get('.q-field > .q-field__inner > .q-field__control > .q-field__control-container > [aria-label="Senha"]').type('t');

    cy.get('.full-width > .q-card__actions > .q-btn:nth-child(1) > .q-btn__wrapper > .q-btn__content').click();

    cy.get('a[href="/dashboard/s"]').click();

    cy.contains('button', 'Atualizar minha senha').click();

    cy.get('.q-field > .q-field__inner > .q-field__control > .q-field__control-container > [aria-label="Nova Senha"]').type('t');
    cy.get('.q-field > .q-field__inner > .q-field__control > .q-field__control-container > [aria-label="Confirme a nova senha"]').type('t');
    cy.contains('button', 'Enviar').click();
    cy.wait(200);
  });
});
