describe('home page', () => {
  before(() => {
    cy.resetDatabase();
  });

  it('should render "politica de privacidade"', () => {
    cy.visit('/');
    cy.contains('a', 'Politica de').click();

    cy.get('.q-layout > .q-page-container > .q-banner > .q-banner__content > .text-grey-9').click();

    cy.get('.q-layout > .q-page-container > .q-banner > .q-banner__content > .text-grey-9').click();

    cy.get('body').contains('POLÍTICA DE PRIVACIDADE');
    cy.get('body').contains('21/01/2021');
  });

  it('should have clickable index in "politica de privacidade"', () => {
    cy.get('.row > .bg-white > .text-bold > .q-mt-none > .cursor-pointer:nth-child(1)').click();
    cy.get('h2').contains('1. DEFINIÇÕES');

    cy.get('.row > .bg-white > .text-bold > .q-mt-none > .cursor-pointer:nth-child(2)').click();
    cy.get('h2').contains('INFORMAÇÕES QUE COLETAMOS');

    cy.get('.row > .bg-white > .text-bold > .q-mt-none > .cursor-pointer:nth-child(3)').click();
    cy.get('h2').contains('COMPARTILHAMENTO DE DADOS');

    cy.get('.row > .bg-white > .text-bold > .q-mt-none > .cursor-pointer:nth-child(4)').click();
    cy.get('h2').contains('PROTEÇÃO E SEGURANÇA');

    cy.get('.row > .bg-white > .text-bold > .q-mt-none > .cursor-pointer:nth-child(5)').click();
    cy.get('h2').contains('PROTEÇÃO E SEGURANÇA');

    cy.get('.row > .bg-white > .text-bold > .q-mt-none > .cursor-pointer:nth-child(6)').click();
    cy.get('h2').contains('DIREITOS DOS TITULARES DOS DADOS PESSOAIS');

    cy.get('.row > .bg-white > .text-bold > .q-mt-none > .cursor-pointer:nth-child(7)').click();
    cy.get('h2').contains('MANUTENÇÃO E EXCLUSÃO DOS DADOS PESSOAIS');
  });

  it('should render "termos de uso"', () => {
    cy.visit('/');
    cy.contains('a', 'Termos e').click();
    cy.get('body').contains('TERMOS E CONDIÇÕES DE USO DE USUÁRIO');
    cy.get('body').contains('21/01/2020');
  });

  it('should have clickable index in "termos de uso"', () => {
    cy.get('.row > .bg-white > .text-bold > .q-mt-none > .cursor-pointer:nth-child(1)').click();
    cy.get('.row > .bg-white > .text-bold > .q-mt-none > .cursor-pointer:nth-child(2)').click();
    cy.get('.row > .bg-white > .text-bold > .q-mt-none > .cursor-pointer:nth-child(3)').click();
    cy.get('.row > .bg-white > .text-bold > .q-mt-none > .cursor-pointer:nth-child(4)').click();
    cy.get('.row > .bg-white > .text-bold > .q-mt-none > .cursor-pointer:nth-child(5)').click();
    cy.get('.row > .bg-white > .text-bold > .q-mt-none > .cursor-pointer:nth-child(6)').click();
    cy.get('.row > .bg-white > .text-bold > .q-mt-none > .cursor-pointer:nth-child(7)').click();
    cy.get('.row > .bg-white > .text-bold > .q-mt-none > .cursor-pointer:nth-child(8)').click();
    cy.get('.row > .bg-white > .text-bold > .q-mt-none > .cursor-pointer:nth-child(9)').click();
    cy.get('.row > .bg-white > .text-bold > .q-mt-none > .cursor-pointer:nth-child(10)').click();

    cy.get('h2').contains('SERVIÇO');
    cy.get('h2').contains('ELEGIBILIDADE');
    cy.get('h2').contains('CADASTRO, CONTA, SENHA E PERFIL DE ATENDENTE');
    cy.get('h2').contains('FUNCIONALIDADES, TESTE E TUTORIAL');
    cy.get('h2').contains('CAPTURA DE DADOS E PRIVACIDADE');
    cy.get('h2').contains('LICENÇA E PROPRIEDADE INTELECTUAL');
    cy.get('h2').contains('DAS OBRIGAÇÕES E RESPONSABILIDES DAS PARTES');
    cy.get('h2').contains('ACESSO À REDE E DISPOSITIVOS');
    cy.get('h2').contains('PAGAMENTO E TARIFAS');
    cy.get('h2').contains('FORO E LEGISLAÇÃO APLICÁVEL');
  });
});
