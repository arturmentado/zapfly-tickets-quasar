let admin;

describe('Tickets Menu', () => {
  before(() => {
    cy.resetDatabase().then(() => {
      cy.createAdmin(true);
    });

    cy.fixture('admin')
      .then((e) => {
        admin = e;
      });
  });

  it('should open ticket room', () => {
    cy.visit('/');
    cy.get('.q-field > .q-field__inner > .q-field__control > .q-field__control-container > [aria-label="E-mail"]').type(admin.email);
    cy.get('.q-field > .q-field__inner > .q-field__control > .q-field__control-container > [aria-label="Senha"]').type(admin.password);
    cy.get('.full-width > .q-card__actions > .q-btn:nth-child(1) > .q-btn__wrapper > .q-btn__content').click();

    cy.sendState(admin.phone, 'open');
    cy.sendMessage(admin.phone);

    cy.get('[href="/dashboard/a"]').first().click();
    cy.get('.sidemenu-scroll .q-item').first().click();
  });

  it('should show and hide ticket room menu', () => {
    cy.contains('.q-item__label', 'Tags');
    cy.contains('.chat .q-btn', 'more_vert').click();
    cy.contains('.q-item__label', 'Tags').should('not.exist');
    cy.contains('.chat .q-btn', 'more_vert').click();
  });

  it('should change chat name', () => {
    cy.contains('.q-item__section--main > :nth-child(1) > .cursor-pointer', 'edit').click();
    cy.get('.q-menu input').clear().type('Este é um teste 999');
    cy.contains('.q-btn', 'Salvar').click();
    cy.contains('body', 'Este é um teste 999');
  });

  it('should CRUD tags', () => {
    cy.contains('#tags_control .q-btn', 'add').click();
    cy.get('input[aria-label="Rótulo"]').type('tagteste0999');
    cy.contains('label', 'Cor de fundo').click();
    cy.get('.q-virtual-scroll__content > :nth-child(2) > .q-item__section > .q-item__label').click();

    cy.contains('.q-btn', 'Criar').click();
    cy.contains('#tags_control', 'tagteste0999');

    cy.get('#tags_control .q-checkbox').click();
    cy.get('#tags_control .q-checkbox__inner--truthy');
    cy.contains('.q-item .q-badge', 'tagteste0999');

    cy.contains('.q-ml-auto > .q-btn > .q-btn__wrapper > .q-btn__content', 'delete').click();
    cy.contains('#tags_control', 'tagteste0999').should('not.exist');
    cy.contains('.q-item .q-badge', 'tagteste0999').should('not.exist');
  });

  it('should add and remove notes', () => {
    cy.get('#notes_control textarea').type('notas teste 0999');
    cy.contains('.q-item .q-badge', 'Nota');

    cy.get('#notes_control textarea').clear();
    cy.contains('.q-item .q-badge', 'Nota').should('not.exist');
  });

  it('should CRUD messages fast', () => {
    cy.contains('#quick_messages_chat .q-btn', 'add').click();
    cy.get('input[aria-label="Titulo"]').type('messagem rapida 0999');
    cy.get('input[aria-label="Atalho"]').type('rapida');
    cy.get('.q-dialog [aria-label="Mensagem"]').type('Esta é uma mensagem rapida 0999');
    cy.contains('.q-btn', 'Criar').click();
    cy.contains('.q-dialog').should('not.exist');
    cy.contains('#quick_messages_chat', 'messagem rapida 0999');

    cy.get('.chat-container [placeholder="Mensagem"]').type('/rapida{enter}');
    cy.get('.chat-container [placeholder="Mensagem"]').should('have.value', 'Esta é uma mensagem rapida 0999');

    cy.contains('#quick_messages_chat .q-btn', 'delete').click();
    cy.contains('#quick_messages_chat', 'messagem rapida 0999').should('not.exist');
  });
});
