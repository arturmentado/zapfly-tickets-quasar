let admin;

describe('Admin Users', () => {
  before(() => {
    cy.resetDatabase().then(() => {
      cy.createAdmin(true);
    });

    cy.fixture('admin')
      .then((e) => {
        admin = e;
      });
  });

  it('should see support menu', () => {
    cy.visit('/');
    cy.get('[aria-label="E-mail"]').type(admin.email);
    cy.get('.q-field > .q-field__inner > .q-field__control > .q-field__control-container > [aria-label="Senha"]').type(admin.password);
    cy.get('.full-width > .q-card__actions > .q-btn:nth-child(1) > .q-btn__wrapper > .q-btn__content').click();
    cy.contains('.q-btn', 'support').click();
    cy.contains('.q-dialog', 'Qual tipo de problema esta tendo');
  });

  it('can choose a option', () => {
    cy.contains('.q-item', 'Duvida').click();
    cy.get('.q-dialog [aria-label="Descrição"]');
  });

  it('need to type 30 characters to send support request', () => {
    cy.get('.q-dialog [aria-label="Descrição"]').type('Something something something');
    cy.contains('.q-btn', 'Pedir suporte').click();
    cy.contains('.q-dialog', 'Precisa ter pelo menos 30 caracteres');
  });

  it('can send support request', () => {
    cy.get('.q-dialog [aria-label="Descrição"]').type(' something something');
    cy.contains('.q-btn', 'Pedir suporte').click();
    cy.contains('Em breve').click();
  });

  it('should created a new ticket to support', () => {
    cy.get('[href="/dashboard/a"]').first().click();
    cy.get('.sidemenu-scroll .q-item').first().click();
    cy.contains('.q-message', 'Pedido de suporte');
    cy.contains('.q-message', 'Está com dúvida de algo? Me fala qual sua duvida e irei chamar um atendente humano.');
    cy.contains('.q-message', 'Form: Something something something something something');
  });

  it('could go multiple layers down', () => {
    cy.contains('.q-btn', 'support').click();
    cy.contains('.q-item', 'Problemas de conexão').click();
    cy.contains('.q-item', 'Deu certo!').click();
    cy.contains('.q-btn', 'arrow_back').click();
    cy.contains('.q-item', 'Deu certo!');
  });
});
