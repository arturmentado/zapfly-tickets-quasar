let admin;

describe('Chat New', () => {
  before(() => {
    cy.resetDatabase().then(() => {
      cy.createAdmin(true);
    });

    cy.fixture('admin')
      .then((e) => {
        admin = e;
      });
  });
  it('should show only running', () => {
    cy.visit('/');
    cy.get('.q-field > .q-field__inner > .q-field__control > .q-field__control-container > [aria-label="E-mail"]').type(admin.email);
    cy.get('.q-field > .q-field__inner > .q-field__control > .q-field__control-container > [aria-label="Senha"]').type(admin.password);
    cy.get('.full-width > .q-card__actions > .q-btn:nth-child(1) > .q-btn__wrapper > .q-btn__content').click();

    cy.sendState(admin.phone, 'CONNECTED');

    cy.get('[href="/dashboard/a"]').first().click();
    cy.contains('.ticket-filter-status', 'Atuais');
  });

  it('should change to all status', () => {
    cy.contains('.q-field__append > .q-btn > .q-btn__wrapper > .q-btn__content > .material-icons', 'more_vert')
      .click();

    cy.contains('.q-item__section', 'Mostrar todos')
      .click();
    cy.contains('.ticket-filter-status', 'Todos')
      .get('.q-chip__icon')
      .click();
    cy.contains('.ticket-filter-status', 'Atuais');
  });

  it('should change to only waiting', () => {
    cy.contains('.q-field__append > .q-btn > .q-btn__wrapper > .q-btn__content > .material-icons', 'more_vert')
      .click();

    cy.contains('.q-item__section', 'Apenas em espera')
      .click();
    cy.contains('.ticket-filter-status', 'Em espera')
      .get('.q-chip__icon')
      .click();
    cy.contains('.ticket-filter-status', 'Atuais');
  });
});
