let admin;

describe('Connection/Tickets settings', () => {
  before(() => {
    cy.resetDatabase().then(() => {
      cy.createAdmin({ addTime: true, addConnection: true });
    });

    cy.fixture('admin')
      .then((e) => {
        admin = e;
      });
    cy.visit('/');
    cy.get('.q-field > .q-field__inner > .q-field__control > .q-field__control-container > [aria-label="E-mail"]').type('teste@teste.com');
    cy.get('.q-field > .q-field__inner > .q-field__control > .q-field__control-container > [aria-label="Senha"]').type('teste');
    cy.get('.full-width > .q-card__actions > .q-btn:nth-child(1) > .q-btn__wrapper > .q-btn__content').click();
  });

  it('connections settings dialog shows', () => {
    cy.get('[href="/dashboard/s"]').click();
    cy.contains('button', 'Configurações das conexões').click();
    cy.contains('.q-dialog', 'Configurações das conexões').should('be.visible');
    cy.contains('.q-dialog button', 'Cancelar').click();
  });

  it('ticket settings dialog shows', () => {
    cy.get('[href="/dashboard/s"]').click();
    cy.contains('button', 'Configurações dos atendimentos').click();
    cy.contains('.q-dialog', 'Configurações dos atendimentos').should('be.visible');
    cy.contains('.q-dialog button', 'Cancelar').click();
  });
});
