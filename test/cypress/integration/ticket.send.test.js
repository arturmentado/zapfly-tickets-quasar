let admin;

describe('Chat Send Messages', () => {
  before(() => {
    cy.resetDatabase().then(() => {
      cy.createAdmin(false);
    });

    cy.fixture('admin')
      .then((e) => {
        admin = e;
      });
  });

  it('should try connect', () => {
    cy.visit('/');
    cy.get('.q-field > .q-field__inner > .q-field__control > .q-field__control-container > [aria-label="E-mail"]').type(admin.email);
    cy.get('.q-field > .q-field__inner > .q-field__control > .q-field__control-container > [aria-label="Senha"]').type(admin.password);
    cy.get('.full-width > .q-card__actions > .q-btn:nth-child(1) > .q-btn__wrapper > .q-btn__content').click();

    cy.contains('button', 'Clique aqui para conectar seu whatsapp').click();
    cy.get('.q-card').contains('Atenção');

    cy.contains('Iniciar').click();

    cy.get('body').contains('Pode levar ate 1 minuto');
  });

  it('should show QR code and scan it', () => {
    cy.sendQrCode(admin.phone);
    cy.get('.q-dialog img');
    cy.get('body').contains('precisa ser autenticada');
    cy.sendState(admin.phone, 'open');

    cy.contains('button', 'Connectar').should('not.exist');
  });

  it('should see chats', () => {
    cy.get('[href="/dashboard/a"]').click();
    cy.get('body').contains('Nenhum atendimento no momento');
    cy.sendMessage(admin.phone);
    cy.get('body').contains('5516996160622');
  });

  it('should see chat room', () => {
    cy.get('.sidemenu-scroll .q-item').first().click();
    cy.get('.q-message-text');
    cy.get('.bg-grey-P');
    cy.get('.col-12 > .q-field > .q-field__inner > .q-field__control');
    cy.get(':nth-child(1) > .full-width > .q-card__section');
    cy.get('#tags_control');
    cy.get('#notes_control > .q-py-none');
    cy.get('#tags_control');
  });

  it('should send message', () => {
    let random = Math.floor(Math.random() * 90000) + 10000;
    cy.get('[placeholder="Mensagem"]')
      .type(random)
      .type('{enter}');
    cy.contains('p', random);

    random = Math.floor(Math.random() * 90000) + 10000;
    cy.get('[placeholder="Mensagem"]')
      .type(random);
    cy.get('#send_btn').click();
    cy.contains('p', random);

    cy.get('.fa').click();
    cy.get('.grid-emojis > :nth-child(1)').click();
  });

  it('should send media dropped', () => {
    cy.get('.messages-container').attachFile('reddot.png', { subjectType: 'drag-n-drop' });
    cy.get('.chat .q-img__image');
    cy.get('[placeholder="Mensagem"]').clear();
    cy.get('#send_btn').click();
    cy.contains('body', 'Enviado!');
    cy.contains('body', 'Enviado!').should('not.exist');
  });

  it('should send media dropped with caption', () => {
    cy.get('.messages-container').attachFile('reddot.png', { subjectType: 'drag-n-drop' });
    cy.get('.chat .q-img__image');
    cy.get('[placeholder="Mensagem"]')
      .type('caption for image');
    cy.get('#send_btn').click();
    cy.contains('body', 'Enviado!');
    cy.contains('body', 'Enviado!').should('not.exist');
  });

  it('should send audio', () => {
    cy.get('#audio_btn > .q-btn__wrapper > .q-btn__content').click();
    cy.contains('.inline > .q-btn > .q-btn__wrapper > .q-btn__content', '0:00');
    cy.wait(100);
    cy.get('.text-negative > .q-btn__wrapper > .q-btn__content > .material-icons').click();

    cy.get('#audio_btn > .q-btn__wrapper > .q-btn__content').click();
    cy.contains('.inline > .q-btn > .q-btn__wrapper > .q-btn__content', '0:00');
    cy.wait(100);
    cy.get('.text-positive > .q-btn__wrapper > .q-btn__content > .material-icons').click();
    cy.contains('body', 'Enviado!');
    cy.contains('body', 'Enviado!').should('not.exist');
  });
});
