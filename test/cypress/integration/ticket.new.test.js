let admin;

describe('Chat New', () => {
  before(() => {
    cy.resetDatabase().then(() => {
      cy.createAdmin(true);
    });

    cy.fixture('admin')
      .then((e) => {
        admin = e;
      });
  });
  it('should send message to a new number', () => {
    cy.visit('/');
    cy.get('.q-field > .q-field__inner > .q-field__control > .q-field__control-container > [aria-label="E-mail"]').type(admin.email);
    cy.get('.q-field > .q-field__inner > .q-field__control > .q-field__control-container > [aria-label="Senha"]').type(admin.password);
    cy.get('.full-width > .q-card__actions > .q-btn:nth-child(1) > .q-btn__wrapper > .q-btn__content').click();

    cy.sendState(admin.phone, 'CONNECTED');

    cy.get('[href="/dashboard/a"]').first().click();
    cy.get('.q-item__label > .q-field > .q-field__inner > .q-field__control > .q-field__append > .q-btn > .q-btn__wrapper > .q-btn__content > .material-icons').click();

    cy.get('.q-menu > .q-list > :nth-child(2) > .q-item__section').click();
    cy.get('.wrap input[type="text"]')
      .type('5555555555555');

    cy.contains('.create-contact', 'Novo contato')
      .click();

    cy.get('textarea')
      .type('Está é uma mensagem de teste');

    cy.contains('.q-btn', 'Enviar')
      .click();
    cy.contains('.q-item.q-item--active', '5555555555555');
    cy.contains('.q-message-text', 'Está é uma mensagem de teste');
  });

  it('should send message to a new number selecting a contact', () => {
    cy.get('.q-item__label > .q-field > .q-field__inner > .q-field__control > .q-field__append > .q-btn > .q-btn__wrapper > .q-btn__content > .material-icons').click();

    cy.get('.q-menu > .q-list > :nth-child(2) > .q-item__section').click();
    cy.get('.wrap input[type="text"]')
      .type('5555555555555');
    cy.contains('.contact-item', '5555555555555')
      .click();

    cy.get('.q-dialog textarea')
      .type('Está é outra mensagem de teste');

    cy.contains('.q-btn', 'Enviar')
      .click();

    cy.contains('.q-message-text', 'Está é outra mensagem de teste');
  });

  it('should send message in mass', () => {
    cy.get('.q-item__label > .q-field > .q-field__inner > .q-field__control > .q-field__append > .q-btn > .q-btn__wrapper > .q-btn__content > .material-icons').click();

    cy.get('.q-menu > .q-list > :nth-child(2) > .q-item__section').click();
    cy.get('.wrap input[type="text"]')
      .clear()
      .type('1');

    cy.contains('.create-contact', 'Novo contato')
      .click();

    cy.get('.wrap input[type="text"]')
      .clear()
      .type('2');

    cy.contains('.create-contact', 'Novo contato')
      .click();

    cy.get('.q-dialog textarea')
      .type('Mensagem em massa');

    cy.contains('.q-btn', 'Enviar')
      .click();
    cy.wait(7100);

    cy.contains('.q-item', '552')
      .click();

    cy.contains('.q-message-text', 'Mensagem em massa');
  });
});
