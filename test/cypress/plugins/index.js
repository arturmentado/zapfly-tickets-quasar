module.exports = (on, config) => {
  // eslint-disable-next-line global-require
  const configWithDotenv = require('dotenv').config();
  let env = process.env;
  if (!configWithDotenv.error) {
    env = { ...config.env, ...configWithDotenv.parsed };
  }
  const result = { ...config, env };
  return result;
};
