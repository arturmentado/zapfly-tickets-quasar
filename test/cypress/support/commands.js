/* eslint-disable linebreak-style */
import 'cypress-file-upload';
import axios from 'axios';

const apiUrl = Cypress.env('API_URL') || 'localhost:3030/';
// const apiUrl = 'https://dev-be.zapfly.com.br';

Cypress.Commands.add('resetDatabase', () => axios.get(`${apiUrl}/admin/test-tools?action=clear_database`)
  .then(console.log)
  .catch(console.error));

Cypress.Commands.add('sendQrCode', (phone) => {
  const base64 = 'iVBORw0KGgoAAAANSUhEUgAAAMgAAADICAYAAACtWK6eAAANpElEQVR4Xu2d/5XkVhGFyxGYDNYbAXYEiyPZdQRABAsRGCLwOhJDBIYIsDOACMyp6R7UQ8t6X82UpNb0N+fMX6quV7qvbt33Q0/6LCJ+Cf8Sgb9HxO+aoEg/PwBfX0fE3wZ21BdoDt9jxvSOOHztNp9JkP91sQSZsl2CnLGQIFNSSBAJciWIEkSCzI2SVBAV5CovVBAVRAVZmFRKEAkiQSQIWqlziOUQyyHWQrGQIBJEgkiQ8S4OXcXK8floQ2vc2n4WHyLizaB5Ogchvt5GxHtwu50bhd9HxE+DNvP6JxAXUZCfoS/Q3C4muQE73AylBPlzRPxpl9voaZR0OCUI8UWj7iQI8UXjIvdI8aJtbm2X+fxx1KgEqe+DkOQZ4f54nSQ1fdSE+KJxkXuUIBdoqiDrLIGSpJYglNY1OxXkAq/Oikh80a6SIBSpfjsJIkGenVWkCDjEcog1m2AkeWhmqiAUqX47FUQFeXZWkSKggqggKsgCxSSIBJEgEoQduaXLvFsf36VxdQ4ZiC86tiFzEOqLxEWrfqcvNNanNwntcn9v9Ifi6t4olCCjbnl6XYLU8KLWEuSMlAoypUxn1e/0hSo1zXxoJ0EkyFWqdCZ1py8JctFVDrFgiTubOcSq4UWtVRAVRAVZYIsEkSASRIKMBdVJupP0uSxRQVQQFUQFuU0FySO3XwxCy+tdR26pL1I1O4/c0k1HV7HubBVrTNvTS7C3fnn1ra6ISRAJcsUZCTJBIkEkiARZkFUJIkEkiARhr/25h5105yAnBJykX2QC3W+QICfQnIM4B5ktpBJEgvx/YjgHcQ7iHMQ5iHMQMvd4tHGI5RDrbodYnWclKqQb2XbG1enLIdadDbE6k2eU9JXrnXF1+pIgEuQqj+kSaIUAI9vOpO70JUEkiARxku4k/TEHOqvrSBUq1zvj6vSlgqggKogKooKoIE9ZQOdZKogKooKoICqICqKC/GodoA8rbv2hz5xMkq/vdk46O32RSTg9cvtN45d8O+8xnyrI/y3/SB6ioV/3u3m3BKHSVmeHd/oi90AfWyG+6Lxh63sksXfbSJALRDs7vNMX6XQJQlCq20gQCfLsxYOti0A9vV/+CwkiQSTIS1fXnINMCN7q+Nwh1svVYs6DCqKCqCAqCKsunWPqTl8kehWEoFS3UUFUEBVEBWGVo7Pqd/oi0asgBKW6TauC/BwR/6rHMPsLujDQ1NyDm68i4vOBwz0m6X+JiN8O4krsPwEwiK/Oe/xPRPwI4urs705fb8GTB9HZIMDqpk06k+cefN10Z3YFJ0H2XebtHK5t7asrB2/ajwSRIHMJSsh204ndFZwEkSASZIFNEkSCSBAJggT3HibWnfeIQD26kQqigqggKgiqY53V9R58IVCPbpQKQo6sHv0+Sfz/iIg/AEOywvPPiPg98PVXsFHY6YsSNzcdvwTxv3oT8ungVw9C8QYJQYouNzOnBNksoFtvSILUe0iC1DE77C8kSL3rJEgds8P+QoLUu06C1DE77C8kSL3rJEgds8P+QoLUu06C1DE77C8kSL3rJEgds8P+QoLUu06C1DE77C8kSL3rJEgds8P+IgnyA4iePrP1NfD1ISLeAzviC7h5MCHHUek9fh8RPw0azl3ob2lwA7vcSSc7/KS5f0dEPjEw+uvEa9TW43XS3zR3SJvZj8OjzDQpSINpQxQJHZaHvmhcnVU/O3L0eM4eL1qgWBC7TrxIe925Q9pEXyyQIATKpzYSpI4Z+UVncSXtSZALlDorogQh6Ve3kSBnzBxi1ZJnjwcMOwsKvVsJIkForjyxkyATHLS4EqAdYjnEInkya6OCnGFxkl7PIecgdczILxxiOcQieXJl4xBr5yHWaE0/w8svrb4B3UuqQG725P/oj3wZlcaVX4BN26W/34Djr/l7FeSEYr6bl2w6jvr58Trpb5o770CjuVH43ciOJHT6oJMj6m8UF71O4+pM6k5f5D5vVUH2iIvglTa/UMORHU1omojU3yguep3G1ZnUnb7Ife6RiGSSvkdcBC8JcoGSBKEpU7OTIGe8aMWniUj91brr161pXJ1Vv9MXwWGPSi1BJMhVbtIHDCXICbo9iEsKikMsh1g0T55tp4KoICrIAn0kiASRIBJkrLB0Uk0nw9TfODJmQePqnDd0+iJ3ucdYXwVZSUHIrjw66kgyp7CBmTu+edx06Y9+TTZ3c0dPFVBf5GgrJQjxRZ+9I8eK6fFd2JWtR7/zSd3RHzq+Sys+rdSjoPI6esyYOCoQhLijidhZXbf2RXBIG6KS1Be1a9v9hse1UU5LkKn7JMiEhQRZaYhFqoUKMqGkgkxYqCBnLCSIBJkrpBJEglzlhQqiglwlhQqigqggC5MRCSJBJIgECXJCziGWQ6wrqqCjjuenRUerYmgtOyLy/bajjcLOr9x2LhnTr9ySY8UjPB+v57uAO4/TknbJJD03YEfvRc62PoIGE6/hO6L32AcBsT+YkNgoQTrX9a36tAdrdoQgdHhOfKHoSBKmI5qIqFFoRGKjcUkQCPqOZiSpJchFB0mQWrZ2FoFayz3WEqSIowSpASZBVpjwkyR0iPU0UZ2D1IhLrVUQitTZjpDXOcgEqgqiglxRTIJIkLm6S9QI1WtSpR1iOcRCyfRCI5LUrmK5ivXsNHOItdIQi3zl9i04ZprhdX6plBzfzR1rYkeOydLjqF9FxOeDNM4XO/8IUp0cbQVuHkzIPdIv5pLju9QXOtoagR7N2UVBiLTRTiJDNjpvIG1SwMjKE2mv26az6pN77HwEhvrao7/bcppWTZoYEoQidbKTIDW8aEGUIGdcKWCkuta6qsdagtRwpP0tQSTIVWaRIkCHRZ2+HGKttPJEagutKKTDSXvdNipIDVHa3yqICqKCLHBLgkgQCSJBxvJLJdch1glL5yDjnHpikcuyZKONuiXnsOmXSkmbnyIi/0d/ufH15cgIXk8/o41C6Aot89Kv75Ijt/RYMcErj74OvxIbEehoKwSMFsTWIRaMTbMzAp1qRCbpnV++6uxEGldnmxKkE82VfEmQE7ASZKUEO7pbCSJBjp7Dq8YvQSTIqgl2dOcSRIIcPYdXjV+CSJBVE+zoziWIBDl6Dq8avwSRIKsm2NGdS5A7Iwg5ctt5sGoPX6RNeoR0a4LkTjp5CoB8yTf9fAsq1B/By6vpPkjnV41z9568vJrspKO4SOIAPF+FSedzShQQspNOfRE7mtQkLuqL7n6T+KkNIQiKS4JMkEuQCQsJcsZCgkiQuaosQSTIVV6oICrIVVKoICqICrIws5EgEkSCSBC0+OEQyyGWQ6wFqkgQCfJsgtCvi6JSvYMROSZLCUKOo9JjsmS1iPoiw+V8rzDZKCRx0X2Qzq8a09Qhx8jRcW0CagaFNlVo9DvYkd1vShASPk2ezkQkcVGbPeIir62l8bfZSZD6JJ2AL0EISk9tJEgds7ZfqCA1KFWQM14qiAqy9jIvpaYKQpFawU4FqYGqgqggVxnjJH2dZV5KTRWEIrWCnQpSA1UFUUFUkAXOSBAJIkEkyFhWu1exPo6bDNomcPXwtnKya7r1EOuL81dnR/eQu7mjI6R0T6Xzi7kZ9+hUXn75+P3oBgvX80tUoz/a3yM/eT1xfTcypMlKd9JHoI7iqV6ncW1NkOp9LNlTgpBhEY2L4EV9ddrR/iZtok/DSZAJys5VLNJB1EaCTEhJEJo1ZzsKGKmIEmQCn+BV7KoWc9rfpDEV5AIl0uESRIJcEcshlkOsuWpLCgqp0t02KkgRUQoY6XAVRAVRQRYIKEEkiASRIEijieIiR81GdMRAmnWS7iSd5MmsjQQ5w+IkvT5Jv9Xk6dwoJMyi+zO06pNN5k5f5B7xYx+bB4ai52flSVLTOQjxBcNvNZMgE5yEbAh8FUQFQYkyY6SCXICigtz+Co8KooJc1TFKXDIscohV0xIVRAU51AqPCqKCqCALRV6CSBAJIkGCHKxyFeucKM5BJsaoICrITSjIh4jII7VLf3m96zhqvjg8j+aSv1HlrPgatUePFaefUVxpQ1/70+XLI7cXPdy5ijVKnLxOV3iIr87VNeqLxEVt0DNPkCCb+3KjcOrmzuSRIBOumyd1J9kkiAShSvBcOwlygRwZHz4X6Lnf7TFJJ/GrICrIbJ5IkBMsEkSCSJAFKZEgEkSCSBAy2nzY2KNv3Bw53NyXk3Qn6aOkfOn1zZPaVaypy5ykT1hsvddDiSNBKFI72nUmz636IvDuMTfqJAi5x1ab7iFWa3CNzm41qTvjInBJEILShY0Eqc9BOpO60xfpeglCUJIgsyjRR006k7rTF+l6CUJQkiASpJgnc+a0oDgHaQB7bRedlfpWfREMVRCCkgqighTzRAUZAEb3GxpwX8XFrVb9zrgIcCoIQUkFUUGKeaKCDADLSke+Jktwp0vLnb6+iYg3A4d00tlZ9YmvPCb7HQCD4EqPtYLmHkzI09t5Vn74NdkI9DIG+pVb8uwX8kVApWAd3e5WCdKJK71H0mbncI20lzZ0qE+Ii3xJkKlraPKQqt/piyYPsaNxEV8ShKD0imxo8kiQU6dLkFeU/ORWJAhBabKRIDW8Dm8tQWpdKEFqeB3eWoLUulCC1PA6vLUEqXWhBKnhdXhrCVLrQglSw+vw1hKk1oV3QZD/ApxzaXJ51NKBAAAAAElFTkSuQmCC';

  const data = [
    null,
    {
      sessionId: phone,
      status: 'qrcode',
      qrcode: base64,
    },
    {
      provider: 'rest',
      headers: {
        type: 'whatsapp_conn',
        session_id: phone,
      },
    },
  ];

  axios.post(`${apiUrl}/admin/test-tools?action=call_method&parameters[]=connection-external&parameters[]=patch`, data)
    .then(console.log)
    .catch(console.error);
});

Cypress.Commands.add('sendState', (phone, status) => {
  const data = [
    null,
    {
      sessionId: phone,
      status,
    },
    {
      provider: 'rest',
      headers: {
        type: 'whatsapp_conn',
        session_id: phone,
      },
    },
  ];

  axios.post(`${apiUrl}/admin/test-tools?action=call_method&parameters[]=connection-external&parameters[]=patch`, data)
    .then(console.log)
    .catch(console.error);
});

// Cypress.Commands.add('sendContact', (phone) => {

//   const contact = {

//     jid: '5555555555555@s.whatsapp.net',

//     name: 'CHANGED HERE!',

//     imgUrl: 'https://pps.whatsapp.net/v/t61.24694-24/120471564_156783316115790_9074561131100572648_n.jpg?oh=8debd7a7f816bcdb4801f5a1e175b811&oe=5F79A78E',

//     id: '5555555555555@c.us',

//     kind: 'chat',

//     formattedName: 'CHANGED HERE!',

//     avatar: 'https://pps.whatsapp.net/v/t61.24694-24/120471564_156783316115790_9074561131100572648_n.jpg?oh=8debd7a7f816bcdb4801f5a1e175b811&oe=5F79A78E',

//   }

//   const data = {

//     phone,

//     type: 'contact',

//     contact,

//   }

//   axios.post('${apiUrl}/api/v2/whatsapp/callback', data)

//     .then(console.log)

//     .catch(console.error)

// })

Cypress.Commands.add('sendMessage', (phone) => {
  const message = {
    key: {
      fromMe: false,
      id: '3EB0AE7D57EA7AC76F4D',
      remoteJid: '5516996160622@s.whatsapp.net',
    },
    message: {
      conversation: 'oi',
    },
    messageTimestamp: 1625166425,
    status: 'READ',
  };

  const data = [
    null,
    [message],

    {
      provider: 'rest',
      headers: {
        type: 'whatsapp_conn',
        session_id: phone,
      },
      mongoose: {
        upsert: 1,
      },
    },
  ];

  axios.post(`${apiUrl}/admin/test-tools?action=call_method&parameters[]=messages-external&parameters[]=patch`, data)
    .then(console.log)
    .catch(console.error);
});

Cypress.Commands.add('createAdmin', (validatedJid = false) => {
  axios.get(`${apiUrl}/admin/test-tools?action=create_admin${validatedJid ? '&validatedJid=true' : ''}`)
    .then(console.log)
    .catch(console.error);
});

// Cypress.Commands.add('setStatusCodeWpStub', (code) => axios.get(`http://localhost:81/set-code/${code}`)
//   .then(console.log)
//   .catch(console.error));

Cypress.Commands.add('clearMessages', () => axios.get(`${apiUrl}/admin/test-tools?action=clear_messages`)
  .then(console.log)
  .catch(console.error));

Cypress.Commands.overwrite('visit', (originalFn, url, options) => originalFn(url, {
  onBeforeLoad(win) {
    Object.defineProperty(win.navigator, 'language', { value: 'pt-BR' });
  },
  ...options,
}));
