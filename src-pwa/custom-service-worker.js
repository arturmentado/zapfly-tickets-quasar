/* eslint-disable no-undef */
/* eslint-disable no-restricted-globals */
/*
 * This file (which will be your service worker)
 * is picked up by the build system ONLY if
 * quasar.conf > pwa > workboxPluginMode is set to "InjectManifest"
 */
import { precacheAndRoute } from 'workbox-precaching';
import localForage from 'localforage';
// import generateSubscribe from '../src/boot/generateWebpush';

// Use with precache injection
precacheAndRoute(self.__WB_MANIFEST);

self.addEventListener('push', async (event) => {
  // debugger;
  event.preventDefault();
  const { notification } = event.data.json();
  console.log('[Service Worker] Push Received.');
  const persisted = await localForage.getItem('activeNotification');
  const activeNotification = self.activeNotification || persisted;
  if (!activeNotification) return;

  const title = notification.title;
  const options = {
    body: notification.body,
    icon: notification.icon,
    badge: notification.icon,
    data: {
      url: notification.url,
    },
  };

  console.log('[Service Worker] Options to use.');

  event.waitUntil(self.registration.showNotification(title, options));
});
self.addEventListener('notificationclick', (event) => {
  event.notification.close();
  event.waitUntil(
    // eslint-disable-next-line no-undef
    clients.matchAll({ type: 'window' }).then((windowClients) => {
      // Check if there is already a window/tab open with the target URL
      for (let i = 0; i < windowClients.length; i += 1) {
        const client = windowClients[i];
        // If so, just focus it.
        if ('focus' in client) {
          return client.focus();
        }
      }
      // // If not, then open the target URL in a new window/tab.
      if (clients.openWindow) {
        return clients.openWindow(event.notification.data.url);
      }
    }),
  );
});

self.addEventListener('message', (event) => {
  const data = JSON.parse(event.data);
  self.activeNotification = data.activeNotification;
  localForage.setItem('activeNotification', data.activeNotification);
});

self.skipWaiting();

self.addEventListener('activate', async () => {
  console.log('Service Worker Activate');
//   // after we've taken over, iterate over all the current clients (windows)
//   const tabs = await self.clients.matchAll({type: 'window'})
//   tabs.forEach((tab) => {
//     // ...and refresh each one of them
//     tab.navigate(tab.url)
//   })
});
self.addEventListener('fetch', (event) => {
  if (event.request.url.match('^.*(\/_redirects\/).*$')) {
    return false;
  }

  return event;
});
// self.addEventListener('pushsubscriptionchange', (event) => {
//   console.log('Subscription expired');
//   event.waitUntil(
//     generateSubscribe(self.registration)
//       .then((data) => {
//         const subscription = JSON.parse(JSON.stringify(data));
//         console.log('Subscribed after expiration');
//         return fetch('register', {
//           method: 'post',
//           headers: {
//             'Content-type': 'application/json',
//           },
//           body: JSON.stringify({
//             endpoint: subscription.endpoint,
//           }),
//         });
//       }),
//   );
// });

// self.addEventListener('activate', (event) => {
//   event.waitUntil(
//     startDb(),
//   );
// });
