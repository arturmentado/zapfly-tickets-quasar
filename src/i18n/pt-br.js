export default {
  commonWord: {
    search: 'Busca',
    connection: 'Conexão | Conexões',
    setting: 'Configuração | Configurações',
    the: '{ptBr} | {ptBr}s',
    read: 'Leia',
    welcome: 'Bem-vindo',
    to: 'a',
    access: 'Acesso',
    painel: 'Painel',
    on: 'em',
    updated: 'Atualizado',
    summary: 'Sumário',
    save: 'Salvar',
    password: 'senha',
    reply: 'Respondendo',
    success: 'Sucesso',
    ticket: 'Atendimento | Atendimentos',
    all: 'Todo | Todos',
    current: 'Atual | Atuais',
    waiting: 'Espera',
    option: 'Opção | Opções',
    new: 'Novo',
    filter: 'Filtrar',
    none: 'Nenhum',
    open: 'Aberto',
    date: 'Data',
    url: 'URL',
    method: 'Metodo',
    error: 'Erro',
    online: 'Online',
    offline: 'Offline',
    create: 'Criar',
    cancel: 'Cancelar',
    send: 'Enviar',
    close: 'Fechar',
    product: 'Produto | Produtos',
    available: 'Disponivel',
    choose: 'Escolher',
    image: 'Imagem',
    price: 'Preço',
    restart: 'Recomeçar',
    optionTitle: 'Opção de chatbot', // TODO - iago pq tem esse titulo ta aqui?
    state: 'Estado',
    yesPlease: 'Sim, por favor',
    no: 'Não',
    yes: 'Sim',
    latency: 'Latencia',
    attention: 'Atenção',
    continue: 'Continuar',
    back: 'Voltar',
    deleted: 'deletado',
    warning: 'aviso',
    delete: 'Deletar',
    moneySymbol: 'R$',
    total: 'Total:',
    title: 'Titulo',
    telephones: 'Telefones',
    emails: 'Emails',
    photo: 'Foto',
    message: 'Mensagem | Mensagens',
    shortcut: 'Atalho',
    result: 'Resultado',
    hour: 'Hora',
    chat: 'Chat | Chats',
    restore: 'Restaurar',
    notDefined: ' - N/D - ',
    attendents: 'Atendentes',
    concluded: 'Concluido',
    confirm: 'Confirmar',
    catalog: 'Catalogo',
    devAPI: 'API Desenvolvedor',
    admin: 'Administração',
    users: 'Usuarios',
    ambient: 'Ambiente',
    exit: 'Sair',
    of: 'de',
    for: 'Para',
    initialize: 'Iniciar',
    undo: 'Desfazer',
    reason: 'Motivo',
    others: 'Outros',
    filters: 'Filtros',
    owner: 'Dono',
    signature: 'Assinatura',
    superior: 'Superior',
    type: 'Tipo',
    simple: 'Simples',
    multiple: 'Multiplos',
    hide: 'Esconder',
    details: 'Detalhes',
  },

  settings: {
    usersSettings: 'Configurações de usuario',
    notificationExtra: 'Ativar notificações externas',
    notificationExtraExplain: 'Isso enviara notificações quando um atendimento demorar mais de 1 minuto para ser respondido. (BETA)',
  },

  form: {
    language: 'Linguagem',
    email: 'E-mail',
    address: 'Endereço',
    password: 'Senha',
    passwordNew: 'Nova Senha',
    name: 'Nome',
    lastName: 'Ultimo nome',
    passwordConfirmation: 'Confirme a senha',
    confirmNewPassoword: 'Confirme a nova senha',
    country: 'Pais',
    whatsappNumber: 'Numero de Whatsapp',
    undefinedValue: '- Ind -',
    description: 'Descrição',
    price: 'Preço',
    payerData: 'Dados do pagador',
    ddd: 'DDD',
    phone: 'Telefone',
    street: 'Logradouro',
    number: 'Numero',
    complement: 'Complemento',
    district: 'Bairro',
    city: 'Cidade',
    state: 'Estado',
    postalCode: 'CEP',
    document: 'Documento | Documentos',
    cpf: 'CPF',
    cardData: 'Dados de cartão',
    birthDate: 'Data de nascimento',
    cardNumber: 'Numero do cartão',
    cardFlag: 'Bandeira',
    cardCvv: 'CVV',
    cardMonth: 'Mes de vencimento',
    cardYear: 'Ano de vencimento',
    updateAccountData: 'Atualizar dados da Conta',
    editMyData: 'Editar meus dados',
  },

  connection: {
    disabled: 'Conexão desativada',
    disableIt: 'Desativar conexão',
    notValidatedJid: 'Conexão Nova',
    versionIncompatible: 'Versao da conexão incompativel, peça para ela ser recriada.',
    connectWhatsapp: 'Clique aqui para conectar seu whatsapp',
    whatsappNotYetConnected: 'Clique aqui para conectar seu whatsapp',
    descStatus: 'desc.',
    addTitle: 'Adicionar uma conexão',
    editTitle: 'Editar uma conexão',
    creationSuccess: 'Conexão criada, você já pode conectar',
    whatsappAlreadyRegistered: 'Numero do whatsapp ja cadastrado use outro',
    batteryStatusUnknow: 'Estado da bateria desconhecido',
    batteryStatusLive: 'Carregando',
    batteryStatusSave: 'Economia de energia',
    batteryStatus: 'Estado da bateria',
    reconnect: 'Reconectar',
    connect: 'Conectar',
    checkPreapproval: 'Reveja a situação da assinatura',
    aboutPreapproval: 'Sobre a assinatura',
    preapprovalNotConfigured: 'Assinatura não configurada',
    preapprovalCreated: 'Assinatura criada',
    preapprovalActive: 'Assinatura Ativa',
    preapprovalWaitingProcess: 'Aguardando processo',
    preapprovalCancel: 'Cancelar assinatura',
    preapprovalConfig: 'Configurar assinatura',
    preapprovalUpdateData: 'Atualizar dados de pagamento',
    reconnectClick: 'Clique para reconectar',
    adminPreapprovalOr: 'ou configurar a assinatura',
    preapprovalRemoveWarning: 'Irei remover sua assinatura desta conexão.',
    preapprovalCancelledSuccess: 'Seu pagamento recorrente foi cancelado',
    latencyUnknow: 'Latencia desconhecido',
    latencyOk: 'Latencia ok',
    latencyHigh: 'Latencia alta',
    preapprovalCancelTitle: 'Configurar assinatura recorrente',
    preapprovalValueLine: 'O valor do plano atualmente é R$ 99.99 por mes.',
    canChange: 'Sujeito a alterações.',
    preapprovalValueAttention: 'não armazena nenhum dos dados aqui preenchidos. Todos são enviados para o gateway de pagamento (pagseguro) para realizar todos os processos de pagamentos de forma segura',
    attentionAccept: 'Ok, entendi, e aceito',
    preapprovalUpdateSign: 'Atualizar Assinatura',
    preapprovalCreateSignature: 'Criar Assinatura',
    preapprovalSignatureUpdated: 'Seu pagamento recorrente foi atualizado',
    preapprovalSignatureCreated: 'Seu pagamento recorrente foi configurado',
    chooseTitle: 'Escolha a conexão',
    settingsOf: 'Configurações das conexões',
    settingDisableAutoImportUnreadMessages: 'Ao conectar, NÃO importar mensagens não lidas',
    settingDisableForceUseHere: 'A conexão NÃO ira forçar o uso quando for trocada',
    settingDisableWaitChats: 'A conexão NÃO irá esperar o celular informar os chats',
    settingActiveChatbot: 'Ativar chatbot.',
    settingBlockDirectToPhone: 'Bloquear acesso direto as mensagems do celular.',
    settingAllowAllReconnect: 'Todos os atendentes podem recriar a conexão.',
    settingApiEnable: 'Ativar api de desenvolvedor.',
    allConnected: 'Tudo conectado',
    partlyConnected: 'Parte das conexões está funcionando, mas não todas.',
    notConnected: 'Nenhuma conexão funcionando',
    qr: 'A conexão:',
    recreate: 'Recriar conexão',
    creating: 'Criando uma conexão. Pode levar ate 1 minuto.',
    internetProblems: 'Problemas na internet?',
    back: 'Opá! Voltou!',
    subscriptionCreated: 'As. Criado em',
    createdWhen: 'Criado em',
    extraTrial: 'Extra Trial',
    number: 'Num',
    port: 'Porta',
    conState: 'Con. Est.',
  },

  attendant: {
    dep: 'dep',
    lastAccess: 'Ultimo acesso',
    editProfile: 'Editar perfil',
    editPassword: 'Editar senha',
    editDepartment: 'Editar departamento',
    attendant: 'Atendente | Atendentes',
    created: 'Novo atendente criado',
    updateDep: 'Atualizar departamento',
    supervisor: 'Gerente',
    updatePassword: 'Atualizar senha',
    updateProfile: 'Atualizar perfil',
  },

  ticket: {
    confirmCloseMultiple: 'Tem certeza que deseja fechar {0} atendimentos?',
    closeMultiple: 'Fechar todos atendimentos selecionados',
    selectMultiple: 'Selecionar varios',
    pushToPipeline: 'Colocar para atendimento',
    preService: 'Pre Atendimento',
    mergeTooltip: 'Mesclar este atendimento ao ultimo concluido',
    menuTooltip: 'Detalhes do atendimento',
    loadMore: 'Carregar mais',
    noMessage: 'Nenhuma mensagem',
    noMessageFound: 'Nenhuma mensagem encontrada',
    found: 'encontrada',
    fileTypeNotAccept: 'Tipo de arquivo não aceito',
    uploadingFile: 'Enviando arquivo ao servidor, por favor aguarde...',
    uploadFileSuccess: 'Enviado!',
    uploadFileError: 'Erro! Algo de errado aconteceu!',
    ticketOnWait: 'Atd. em espera',
    createNew: 'Novo atendimento',
    messagesInMass: 'Enviar mensagens em massa',
    searchMessages: 'Buscar mensagens',
    byResponsible: 'Por responsavel',
    byDepartment: 'Por departamento',
    onlyActual: 'Apenas atuais',
    onlyWait: 'Apenas em espera',
    showAll: 'Mostrar todos',
    onWait: 'Em espera',
    atMoment: 'no momento',
    searchAllTickets: 'Buscar todos os atendimentos',
    createTicket: 'Criar atendimento',
    clickOnATicket: 'Clique em um atendimento',
    toSeeHistoric: 'para ver o historico',
    waiting: 'Aguardando',
    shortWaiting: 'Aguard.',
    beingAttended: 'Sendo atendido',
    closed: 'Encerrado',
    beginning: 'Início',
    attendedBy: 'Atendido por',
    noAttendant: 'Nenhum atendente responsavel',
    inWaiting: 'Por em aguardo',
    end: 'Encerrar Atd',
    seePhoneMessages: 'Visualizar mensagens diretamente do celular',
    phoneMessages: 'Mensagens no celular',
    related: 'Atendimentos relacionados',
    past: 'Atendimentos passados',
    note: 'Nota | Notas',
    putWait: 'Por em espera',
    restore: 'Restaurar atendimento',
    close: 'Fechar atendimento',
    reopen: 'Reabrir atendimento',
    reopenThis: 'Reabrir este',
    markRead: 'Marcar como lido',
    ticketsHourRelation: 'Atendimentos x Horas',
    higherValue: 'Maior valor',
    insufficientData: 'Informações indisponiveis ou insuficientes',
    new: 'Novos atendimentos',
    noDepartamentDefined: 'Atendimentos sem um departamento',
    noAttendentDefined: 'Atendimentos sem um atendente',
    follow: 'Acompanhar atendimentos',
    inProgress: 'Em andamento',
    concluded: 'Concluidos',
    merged: 'Mesclado',
    waitingTicket: 'Atd. em espera',
    startNew: 'Começar um novo atendimento',
    massMessages: 'Envio de mensagens em massa',
    addCSV: 'Adicionar CSV para usar no envio',
    selectContact: 'Selecione pelo menos um contato, antes de enviar',
    sendingMessage: 'Aguarde, enviando a mensagem',
    isAbleToAttend: 'Disponivel/Indisponivel para receber atendimentos',
    recreateNotify: 'Recriar notificação',
    askForSupport: 'Pedir suporte',
    informNumber: 'Informe o numero do seu whatsapp',
    supportConfirmation: 'Em breve iremos lhe enviar uma mensagem no whatsapp.',
    supportError: 'Um erro aconteceu. Envie uma mensagem no whatsapp diretamente para 16 99795-7988',
    supportNotValidated: 'Esta conexão não foi validada ainda. Envie uma mensagem no whatsapp diretamente para 16 99795-7988',
    attendantOnCharge: 'Atd responsavel',
    alreadyOpen: 'Já existe um atendimento aberto. Conclua ele primeiro.',
    canCreateNew: 'O correto seria abrir um novo. Posso criar um novo?',
    createNewLabel: 'Criar um novo',
    closedTicket: 'Atendimento fechado.',
    concludeFirst: 'Um atendimento para este contato está em progresso, conclua ele primeiramente',
    restored: 'Atendimento restaurado',
    waitingReason: 'Informe o que estão aguardando',
    onHold: 'Atendimento em espera',
    createdAfter: 'Criado depois de',
    createdBefore: 'Criado antes de',
    closing: 'Atendimento sendo fechado',
  },

  api: {
    title: 'API',
    subtitle: 'Api para desenvolvedores',
    connection: 'Conexão | Conexões',
    documentation: 'Documentação',
    howtoUse: 'Veja como integrar',
    seeHowTo: 'Veja como enviar e receber informações da conexão',
    generalInfo: 'Informações gerais',
    generalInfo1: 'Essas informações devem ser usadas por desenvolvedores que desejam enviar e receber informações de suas conexões de whatsapp.',
    generalInfo2: 'Importante lembrar que qualquer abuso, cancelamento de numero, ou qualquer problema que venha a surgir disto serao resposabilidade do administrador da conta.',
    generalInfo3: 'Tambem podemos a qualquer momento bloquear o uso da api caso detectamos qualquer problema ou abuso.',
    basicWork: 'Funcionamento basico',
    basicWork1: 'ao enviar uma mensagem pela api, ela irá automaticamente adicionar a mensagem na plataforma com os devidas informações como, qual o contato que recebeu, qual o atendimento relacionado, se necessario, a criacao de um contato, ou validacao do numero, criar ou atualizar o atendimento.',
    basicWork2: 'Apos esse processo, será enviado a conexão a mensagem para ser enviado, e a partir daqui o processo se torna assincrono, e a conexão pode falhar de entregar a mensagem caso ela não encontre o celular.',
    basicWork3: 'Por isso, para testes, é recomendado manter o celular disponivel.',
    basicWork4: 'É importante lembrar tambem, que por conta da assincronisidade de todos os sistemas envolvidos, se no caso citado acima, onde a mensagem falha de entregar por falta da comunicacao entre celular e conexão, é possivel que um atendente peça a reconexão pela plataforma, neste caso a mensagem nunca sera enviada. Por tal, testes e a educacao dos atendentes da conexão é importante afim de recriar a conexão ser o ultimo recurso.',
    webhookWork: 'Funcionamento do webhook',
    webhookWork1: 'ao receber certas informações, enquanto uma conexão possuir a API ativa e um WebHook configurado, a plataforma ira enviar para o webhook exatamente a informacão recebida da conexão, este processo pode não ser imediato, pois existe um proxy assincrono no meio do caminho, que ira continuar a tentar entregar a mensagem, afim de não travar a plataforma. Recomendamos um webhook preparado para receber diversos [POST], pois cada atualização de uma mensagem ira ser um novo post. Este numero podendo chegar a 5 requests por mensagem.',
    webhookWork2: 'Recomendado um extensivo periodo de testes para garantir o bom funcionamento.',
    sendMessageTitle: 'Enviar mensagens de texto',
    aboutWorking: 'Informações sobre funcionamento',
    sendMessageDesc: 'Use este comportamento para enviar mensagens de texto.',
    aboutUrlParams: 'Informações sobre cada parametro da URL',
    tokenParam: 'Token',
    connctionParam: 'connection',
    tokenParamDesc: 'Precisa ser gerado nesta pagina, e usado na url',
    connectionParamDesc: 'O numero do whatsapp cadastrado na plataforma',
    aboutPayloadParams: 'Informações sobre cada parametro do payload',
    toParamDesc: 'Numero valido do whatsapp. A plataforma irá validar o numero antes de enviar, alguns numeros de alguns estados ainda não possuem o 9 extra, porem o whatsapp se perde e marca o numero como valido.',
    bodyParamDesc: 'O texto da mensagem a ser enviado',
    sendMediaTitle: 'Enviar mensagens com midia',
    sendMediaDesc: 'Use este comportamento para enviar midias como PDF, AUDIO ou FOTOS. O payload precisa ser um formdata.',
    fileParamDesc: 'Arquivo a ser enviado',
    numberRegistered: 'Numero cadastrado',
    connected: 'Conectado',
    connectedNot: 'Não Conectado',
    webhookWhatIs: 'O que é webhook?',
    webhookDesc1: 'Quando certas informações forem recebidos pelo',
    webhookDesc2: 'ela ira enviar para você atravez desta url.',
    invalidUrl: 'Essa url não é valida.',
    requestData: 'Informações passadas',
    requestQuery: 'Informações de URL',
    responseLabel: 'Informações de URL',
    something: 'Something/algumacoisa',
  },

  setting: {
    confirmUseMultidevice: 'Atenção: Este recurso está em beta, e por tal você pode ter problemas ao usa-lo. Tambem ao ativar ou desativar essa opção você precisará escanear um novo QRCode',
    activeChatbotButtons: 'Ativar botões para o chatbot',
    title: 'Configurações',
    useMultidevice: 'Usar multi devices (BETA)',
    subtitle: 'Configure sua assinatura, e-mail, senha e mais.',
    activeChatbot: 'Ative chatbot',
    tickets: 'Configurações dos atendimentos',
    attendentsCanChangeResponsible: 'Atendentes podem mudar responsavel',
    attendantsCanSeeAllHistorics: 'Atendentes podem ver todos historico de atendimentos',
    toggleAutoQueue: 'Ativar sistema de fila automatica',
    attendantsSeeOnlyOwn: 'Atendentes podem ver APENAS proprios atendimentos',
    toggleCatalog: 'Ativar o meu catalogo de produtos',
    toggleMarkasRead: 'Visualizar um atendimento as mensagens serão marcadas como lidas',
    toggleAllowFastReopen: 'Permitir reabertura rapida de atendimentos',
    installAPP: 'Instalar o APP',
    chosenNotInstall: 'Você escolheu não instalar',
    installing: 'Instalando...',
    logout: 'Você saiu',
    notificationSounds: 'Tocar/Não tocar um som com as notificações',
    lesser: 'Configurações menores',
    lesserControls: 'Configurações pequenas e seus controles',
    onlyOnline: 'Apenas online',
    userDetails: 'Detalhes do usuario',
    completeObject: 'Obj completo',
    simpleMode: 'Ativar modo simples (chats) (não recomendado).',
  },

  chatbot: {
    title: 'Chatbot',
    subtitle: 'Configure mensagens automaticas e menus.',
    save: 'Salvar chatbot',
    fallbackTitle: 'Mensagem de erro e opção invalida',
    fallbackDesc: 'Esta mensagem será usada quando o chatbot não entender a mensagem. Como uma foto, audio ou algo do tipo for enviado para ele.',
    fallbackLabel: 'Mensagem de Fallback',
    fallbackDefault: '[bot] Desculpe, não pude entender está mensagem ou opção. Pode tentar de novo?',
    introTitle: 'Introdução',
    introDefault: '[bot] Olá, sou o chatbot.\nEscolha uma das opções:',
    introDesc: 'Esta mensagem será no inicio da conversa, construa suas opções a partir daqui. O chatbot so responde a mensagens de texto. Irá ignorar todas as mensanges de audio e midias',
    optionsAmount: 'Opções qtd',
    trigger: 'Gatilho',
    triggerOption: 'Gatilho da opção',
    optionMessage: 'Texto da opção',
    optionMessage2: 'Mensagem de resposta',
    createOption: 'Criar opção',
    needToSave: 'É preciso salvar antes.',
    optionText: 'Texto de opção',
    deleteOption: 'Apagar a opção?',
    talkTo: 'Falar com:',
    multipleActions: 'Multiplas ações',
    actionsPerformed: 'Ações que executarei',
    selectAllInfo: 'Selecione todas as informações',
    TalkIn: 'Falar em:',
    buttonsWarning: 'Aviso: o App Whatsapp apenas mostra três botões disponíveis no celular, caso use mais que isso recomendamos não utilizar essa opção.',
  },

  catalog: {
    title: 'Controles do catalogo',
    ourUrl: 'Sua URL',
    formName: 'Nome da URL',
    formNameComplete: 'Nome completo',
    formWaitTime: 'Tempo de espera',
    formDeliveryPrice: 'Preço de entrega',
    formImageCover: 'Imagem de fundo',
    formImageAvatar: 'Imagem principal',
    notCreated: 'Seu catalogo ainda não foi criado',
    createCatalog: 'Criar meu catalogo',
    nextStepTitle: 'Proximos passos',
    nextStepDesc: 'Agora que você tem um catalogo, precisa configurar, preencha os campos que quiser ali do lado, e não esqueça de colocar categorias e produtos.',
    addCategory: 'Adicionar categoria',
    categoryFrom: 'Categorias do catalogo',
    category: 'Categoria | Categorias',
    waitTimeDefault: '20 min a 1 hora',
    deliveryPriceDefault: 'a partir de R$ 1,00',
    urlUnavailable: 'O nome da url não disponivel, escolha outro.',
    alreadyExist: 'Já existe um catalogo com esse nome, escolha outro.',
    product: 'Produtos do catalogo',
    eraseProduct: 'Apagar o produto?',
    unitMeasure: 'Unidade de Medida',
    unitMeasureUn: 'Un',
    unitMeasureKg: 'Kg',
    unitMeasureG: 'g',
    unitMeasurePc: 'Pç',
    uniMetric: 'Uni. Medida',
    desc: 'Desc.',
    productFormTitle: 'Cadastro/Edição de produtos do catalogo',
  },

  notifications: {
    newMessage: 'Nova mensagem de',
    negative: 'Um erro aconteceu',
  },

  validations: {
    equals: 'O valor precisa ser igual ao \'{field}\'',
    lesserThan: 'The value must be lesser than {value}',
    lesserThanOrEqual: 'The value must be lesser than or equal to {value}',
    greaterThan: 'The value must be greater than {value}',
    greaterThanOrEqual: 'The value must be greater than or equal to {value}',
    minLength: 'Precisa ter pelo menos {value} caracteres',
    email: 'Entre um email valido.',
    url: 'Entre uma url valida',
    invalidUrl: 'Essa url não é valida.',
    neededField: 'Campo obrigatorio',
  },

  termsUsage: {
    title: 'Termos e Condições de Uso',
    titlePage: 'TERMOS E CONDIÇÕES DE USO DE USUÁRIO',
    accept: 'Você precisa aceitar os seguintes termos',
  },

  politicPrivacy: {
    title: 'Politica de Privacidade',
    titlePage: 'POLÍTICA DE PRIVACIDADE',
  },

  forgotPassword: {
    invalidTokenError: 'Token invalido, verifique seu email ou peça um novo',
    updatedMessage: 'Sua senha foi atualizada',
    update: 'Atualizar minha senha',
  },

  publicPages: {
    limitedAccess: 'Acesso limitado',
    registerError: 'Um erro aconteceu, verifique dados informados e tente novamente',
    registerUserError: 'Email ou numero de whatsapp já registrado use outro',
    registerEmailError: 'Email já registrado use outro',
    registerJidError: 'Numero do whatsapp já registrado, use outro',
    didNotAcceptPolitics: 'Você precisa aceitar os termos de uso para usar a plataforma',
    didNotAcceptPolicy: 'Você precisa aceitar a política de privacidade para usar a plataforma',
    loginFailed: 'Um erro aconteceu, verifique email e senha e tente novamente',
    forgotPasswordValidationFailed: 'Você precisa fornecer um email valido.',
    forgotPasswordSuccess: 'Um email foi enviado para seu e-mail',
    forgotPasswordFail: 'Um erro aconteceu, verifique se o email',
    iDeclareHaveRead: 'Eu declaro que li e aceito os',
    forgotMyPassword: 'Esqueci minha senha',
    phrase1: 'Realize todos os atendimentos em um só lugar.',
    phrase2: 'Faça um teste agora mesmo!',
    enterData: 'Entre com seus dados de acesso',
    createRegister: 'Criar registro',
    createAccount: 'Criar conta',
    iHaveAccount: 'Já tenho conta',
    noDataLabel: 'Nada aqui ainda',
    noResultsLabel: 'Não achei nada com essa busca',
    maintenance: 'Estamos em manutenção voltamos em breve.',
    offline: 'Você está offline, conecte-se a internet para usar.',
  },

  contact: {
    searchContact: 'Pesquisar contato',
    allNumbersAreValid: 'Todos os numeros são validos',
    numberNotValid: 'Um ou mais numeros não são validos no whatsapp.',
    newContact: 'Novo contato',
    informValidNumber: 'Informe um celular valido',
  },

  departament: {
    setting: 'Configurar departamentos',
    title: 'Departamento | Departamentos',
    add: 'Adicionar departamento',
    notDefined: 'Departamento não definido',
    short: 'Dep',
  },

  message: {
    tooMuchItens: 'Você tem muitas mensagens rapidas, delete algumas para ver todas aqui',
    buttonsMessage: 'Mensagem de botões',
    messageButtonsResponse: 'Uma resposta em forma de botão foi escolhido',
    deleted: 'Esta mensagem foi apagada',
    botEnd: '[bot] Fim de atendimento',
    programatedSend1: 'Essa mensagem foi enviada automaticamente pelo',
    programatedSend2: 'sistema de envio programado',
    cryptography: 'Conversa com criptografia de ponta a ponta',
    missedCall: 'Uma ligação de voz foi perdida',
    missedVideoCall: 'Uma ligação de video foi perdida',
    callRequest: 'Um pedido de ligação foi recebido (veja o celular)',
    answer: 'Responder',
    answering: 'Respondendo',
    copy: 'Copiar mensagem',
    orderedProducts: 'Produtos pedidos:',
    of: 'Mensagem de',
    fallback: 'fallback',
    chatbotOptionsChosen: 'Chatbot opções escolhidas:',
    contactInfo: 'estou enviando os dados de um contato',
    organization: 'Organização:',
    contactInfoError: 'estou enviando os dados de um contato mas não foi possivel mostra-los, entre em contato com o suporte',
    localization: 'estou enviando uma localização',
    live: 'em tempo real',
    gmaps: 'Ver no GMaps',
    stickers: 'Stickers podem não ser mostrados corretamente.',
    mediaSent: 'Enviei o arquivo',
    clickToSee: 'clique aqui para baixar e ve-lo',
    updatingMedia: 'Atualizando media',
    orderPayment: 'Pedido de pagamento',
    orderedBy: 'Solicitado a',
    myNumber: 'meu numero',
    expires: 'Expira em',
    pending: 'PENDENTE',
    quick: 'Mensagens Rapidas',
    deleteQuick: 'Deleta a mensagem rapida',
    createQuick: 'Criar mensagens rapidas',
    titleQuick: 'Escreva um titulo para facilitar',
    shortcutQuick: 'Um atalho para ser usado ao digitar a mensagem',
    messageQuick: 'A mensagem que deve ser usada',
    programated: 'Mensagens programadas',
    programatedShow: 'Mostrar mensagens programadas',
    programatedDelete: 'Deleta a mensagem rapida',
    programatedCreate: 'Programar mensagens para envio',
    sendDay: 'Dia para envio',
    messageUsageHint: 'A mensagem que deve ser usada',
    writeSomething: 'Escreva algo para ver aqui',
    quickFormErrorTitle: 'Preencha todos os campos, (Titulo)',
    quickFormErrorShortcut: 'Preencha todos os campos, (Atalho)',
    sentAudio: 'Enviou um audio',
    sentStick: 'Enviou um stick',
    sentVideo: 'Enviou um video',
    sentImage: 'Enviou uma imagem',
    sentDoc: 'Enviou um documento',
    sentContact: 'Enviou um contato',
    sentLiveLoc: 'Enviou a localização em tempo real',
    sentLoc: 'Enviou a localização',
    triedCall: 'Tentou ligar com audio',
    triedVideoCall: 'Tentou ligar com video',
    isCalling: 'Está ligando',
    catalogOrder: 'Fez um pedido no catalogo',
    sentPaymentInfo: 'Enviou informações de pagamento',
    audioSpeed: 'Velocidade do audio',
    defaultConfirmBtn: 'Confirma a ação?',
    sendMultipleAttachments: 'Enviar multiplos anexos',
    canBeSent: 'Pode ser enviado',
    wontBeSent: 'Não será enviado',
    waitSendingMedia: 'Aguarde, enviando a midia',
    readingFile: 'Aguarde, estamos lendo o arquivo selecionado',
    signMessages: 'Assinar minhas mensagens',
    signMessagesAtEnd: 'Assinar minhas mensagens no final',
    messageSignExplain: 'Se ativo, quando você enviar uma mensagem, usarei sua assinatura junto da mensagem',
    messageSignEndExplain: 'Se ativo, quando você enviar uma mensagem, irei adicionar assinatura ao final da mensagem',
    personalizeSignature: 'Personalizar assinatura',
    signatureHint: 'Modifique apenas se quiser',
    signatureText: 'Usarei este texto para criar sua assinatura',
    exampleMessage: 'Esta é uma mensagem de exemplo!',
    showVideo: 'Mostrar video',
    mediaFail: 'Não foi possivel acessar a midia',
    noneFound: 'Nenhuma mensagem encontrada',
  },

  payment: {
    checkSituation: 'Confira a situação de pagamento.',
    verify: 'Verif. Pgt',
    verifyPayment: 'Verifique o pagamento da conexão',
    paidSubscription: 'O acesso está liberado. Uma assinatura está ativa',
    freeAcess: 'O acesso está liberado.',
    limitedAcess: 'O acesso está limitado. Um pagamento é necessario.',
    limitedNextSub: 'O acesso está limitado. Será liberado na proxima mensalidade. Caso precise agora entre em contato com o suporte (16) 99157-8481 via whatsapp',
    add30Days: 'Adicione 30 dias',
    freedUntil: 'Liberado até',
    freedUnd: 'Liberado indefinidamente',
    mySignature: 'Minha assinatura',
    configureMySignature: 'Configurar Assinatura',
    negated: 'Cancelado ou não aprovado',
    loading: 'Carregando sistema de pagamentos...',
    about: 'Sobre seu pagamento',
  },

  subscription: {
    configPagSeguro: 'Configurar assinatura com PagSeguro',
    configStripe: 'Configurar assinatura com Stripe',
    stripe: 'Sua assinatura via Stripe está ativa, e iremos liberar o acesso logo',
    stripeError: 'Sua assinatura via Stripe não foi concluida ou não aprovada',
  },

  tag: {
    title: 'Tags',
    create: 'Criar tag',
    label: 'Rótulo',
    bgColor: 'Cor de fundo',
    colorPrimary: 'Primaria',
    colorSecondary: 'Secundária',
    colorAccent: 'Acento',
    colorDark: 'Escuro',
    colorPositive: 'Positivo',
    colorNegative: 'Negativo',
    colorInfo: 'Informativo',
    colorWarning: 'Aviso',
    colorRed: 'Vermelho',
    colorPink: 'Pink',
    colorPurple: 'Roxo',
    colorDeepPurple: 'Roxo profundo',
    colorIndigo: 'Indigo',
    colorBlue: 'Azul',
    colorLightBlue: 'Azul leve',
    colorCyan: 'Ciano',
    colorTeal: 'Cerceta',
    colorGreen: 'Verde',
    colorLightGreen: 'Verde leve',
    colorYellow: 'Amarelo',
    colorAmber: 'Amber',
    colorOrange: 'Laranja',
    colorDeepOrange: 'Laranja profundo',
    colorBrown: 'Marrom',
    colorGrey: 'Cinza',
    colorBlueGrey: 'Cinza azul',
    formLabelError: 'Preencha todos os campos, (Rotulo)',
    formColorError: 'Preencha todos os campos, (Cor)',
    formTextColorError: 'Preencha todos os campos, (Cor de texto)',
    delete: 'Deleta a tag',
  },

  reconnectionMessage: {
    open: 'No celular, abra o seu WhatsApp.',
    webWhats: 'Clique nos "..." (Três pontinhos) no canto direito e clique em Whatsapp Web.',
    camera: 'Assim que a Câmera para ler o QRCode estiver aberta, clique em CONTINUAR',
    atention: 'Atenção',
    after: 'Após a conexão estar feita, NÃO ACESSE O WHATSAPP WEB, pois ele desconectará nosso sistema.',
    use: 'O uso, e concequencias do uso da plataforma são de responsabilidade do usuario.',
    block: 'Existe a possibilidade de bloqueio de seu numero, e por tal ao clicar em iniciar você entende e aceita os riscos.',
  },

  date: {
    manyYears: 'anos atras',
    aYearAgo: 'um ano atras',
    monthsAgo: 'meses atras',
    aMonthAgo: 'um mes atras',
    days: 'dias',
    daysAgo: 'dias atras',
    aDayAgo: 'um dia atras',
    hoursAgo: 'horas atras',
    aHourAgo: 'uma hora atras',
    minutesAgo: 'minutos atras',
    aMinuteAgo: 'um minuto atras',
    fewSeconds: 'a alguns segundos',
  },

  linkify: {
    copied: 'Copiado',
  },

  ads: {
    explain: 'Como você não ativou uma assinatura, usamos ads para manter o sistema funcionando',
    whatIs: 'O que é vendo isto?',
  },

  acessMenu: {
    directChat: 'Acesso direto',
    helpWarnings: 'Ajudas e avisos',
  },

  tooltip: {
    close: 'Fechar e marcar como lido',
    seeMore: 'Ver mais',
    seeLess: 'Ver menos',
    none: 'Nenhuma ajuda ou aviso agora',
    notify: 'Lhe avisamos caso algo surja',
  },

  chat: {
    new: 'Novo chat',
    click: 'Clique em um chat',
    seeHistory: 'para ver o historico',
    answering: 'Respondendo a',
  },

  weekDays: {
    monday: 'Segunda',
    tuesday: 'Terça',
    wednesday: 'Quarta',
    thursday: 'Quinta',
    friday: 'Sexta',
    saturday: 'Sábado',
    sunday: 'Domingo',
  },
};
