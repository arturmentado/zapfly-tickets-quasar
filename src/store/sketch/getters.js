export function findIndex(state) {
  return (id) => {
    const index = state.sketchs.findIndex((e) => e.id === id);
    return index;
  };
}
export function getById(state) {
  return (id) => {
    const index = findIndex(state)(id);
    if (index < 0) return null;
    return state.sketchs[index];
  };
}
