import { findIndex } from './getters';

export function save(state, {
  id,
  message,
  reply,
  file,
}) {
  const index = findIndex(state)(id);
  if (file) file = new File([file], file.name);

  if (index > -1) {
    state.sketchs[index].message = message;
    state.sketchs[index].reply = reply;
    state.sketchs[index].file = file;
    return;
  }

  state.sketchs.push({
    id, message, reply, file,
  });
}
