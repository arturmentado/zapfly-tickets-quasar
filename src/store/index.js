import Vue from 'vue';
import Vuex from 'vuex';
import LogRocket from 'logrocket';
import createPlugin from 'logrocket-vuex';

import directAccess from './directAccess';
import sketch from './sketch';
import { makeAuthPlugin, FeathersVuex } from '../boot/feathers-vuex';
// import media from './media';

const logrocketPlugin = createPlugin(LogRocket);

const auth = makeAuthPlugin({ userService: 'members' });

const requireModule = require.context(
  './services',
  false,
  /\.js$/,
);

const servicePlugins = requireModule
  .keys()
  .map((modulePath) => requireModule(modulePath).default);

Vue.use(Vuex);
Vue.use(FeathersVuex);

export default function (/* { ssrContext } */) {
  const Store = new Vuex.Store({
    modules: {
      directAccess,
      sketch,
    },

    plugins: [
      ...servicePlugins,
      logrocketPlugin,
      auth,
    ],

    state: {
      baseUrl: process.env.API_URL ?? 'https://beta-be.zapfly.com.br',
      isPwa: window.matchMedia('(display-mode: standalone)').matches,
      canUsePagseguro: false,
      pwaInstall: null,
      connected: false,
    },

    mutations: {
      pwaInstall(state, data) {
        state.pwaInstall = data;
      },
      connected(state, data) {
        state.connected = data;
      },
      canUsePagseguro(state, data) {
        state.canUsePagseguro = data;
      },
    },

    strict: process.env.DEBUGGING,
  });

  return Store;
// export default Store;
}
