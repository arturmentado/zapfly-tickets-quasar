import feathersClient, { makeServicePlugin, BaseModel } from '../../boot/feathers-vuex';

class MemberConnections extends BaseModel {
  // eslint-disable-next-line no-useless-constructor
  constructor(data, options) {
    super(data, options);
  }

  // Required for $FeathersVuex plugin to work after production transpile.
  static modelName = 'MemberConnections'

  // static namespace = 'connections'

  // Define default properties here
  static instanceDefaults() {
    return {
      email: '',
      password: '',
    };
  }
}
const servicePath = 'member-connections';
const servicePlugin = makeServicePlugin({
  Model: MemberConnections,
  service: feathersClient.service(servicePath),
  servicePath,
  // namespace: 'connections'
});

// Setup the client-side Feathers hooks.
feathersClient.service(servicePath).hooks({
  before: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
});

export default servicePlugin;
