import feathersClient, { makeServicePlugin, BaseModel } from '../../boot/feathers-vuex';

class MemberTickets extends BaseModel {
  // eslint-disable-next-line no-useless-constructor
  constructor(data, options) {
    super(data, options);
  }

  // Required for $FeathersVuex plugin to work after production transpile.
  static modelName = 'MemberTickets'

  // static namespace = 'connections'

  // Define default properties here
  static instanceDefaults() {
    return {
      contact: {
        overrideName: '',
        tags: [],
      },
    };
  }
}
const servicePath = 'member-tickets';
const servicePlugin = makeServicePlugin({
  Model: MemberTickets,
  service: feathersClient.service(servicePath),
  servicePath,
  // namespace: 'connections'
});

// Setup the client-side Feathers hooks.
feathersClient.service(servicePath).hooks({
  before: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
});

export default servicePlugin;
