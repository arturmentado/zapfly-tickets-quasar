import feathersClient, { makeServicePlugin, BaseModel } from '../../boot/feathers-vuex';

class MemberMessages extends BaseModel {
  // eslint-disable-next-line no-useless-constructor
  constructor(data, options) {
    super(data, options);
  }

  // Required for $FeathersVuex plugin to work after production transpile.
  static modelName = 'MemberMessages'

  // static namespace = 'connections'

  // Define default properties here
  static instanceDefaults() {
    return {
    };
  }
}
const servicePath = 'member-messages';
const servicePlugin = makeServicePlugin({
  Model: MemberMessages,
  service: feathersClient.service(servicePath),
  servicePath,
  // namespace: 'connections'
  // handleEvents: {
  //   created: (item, ctx) => {
  //     console.log(ctx);

  //     // const audio = new Audio('/not.ogg', 100, true);
  //     // audio.play();
  //   },
  // },
});

// Setup the client-side Feathers hooks.
feathersClient.service(servicePath).hooks({
  before: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
  after: {
    all: [],
    find: [],
    get: [],
    create: [
    ],
    update: [],
    patch: [],
    remove: [],
  },
  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
});

export default servicePlugin;
