import feathersClient from '../../boot/feathers-vuex';

const handlingIds = [];
function delay(time) {
  return new Promise((resolve) => {
    setTimeout(resolve, time);
  });
}

async function onMemberMessagesPatched(ctx, event) {
  while (handlingIds.includes(event.key.id)) {
    await delay(100);
  }

  handlingIds.push(event.key.id);
  const indexId = handlingIds.indexOf(event.key.id);

  if (!event.key.fromMe) {
    event.messageTimestamp += 100;
  }

  const index = ctx.state.messages.list.findIndex((e) => e.key.id === event.key.id);
  let contact = ctx.rootGetters['member-contacts/get'](event.contactId);

  const jid = event.key.remoteJid;

  if (!contact) {
    contact = await ctx.dispatch('member-contacts/get', [event.contactId], { root: true });
  }

  event.jid = jid;

  const checkChat = ctx.getters.chat(jid);
  if (!checkChat) {
    await ctx.dispatch('chat', { jid, connectionId: contact.connectionId });
  }

  if (index < 0) {
    // if (ctx.getters.lastMessage(jid).messageTimestamp > event.messageTimestamp) return;
    ctx.commit('messagePush', event);

    const item = ctx.getters.chat(jid);
    let count = Number(item.count) + 1;
    if (event.key.fromMe) count = 0;

    ctx.commit('chatSetCount', { jid, count });
    ctx.commit('chatSetT', { jid, t: new Date().getTime() / 1000 });

    handlingIds.splice(indexId, 1);

    return;
  }
  ctx.commit('messageUpdateIndex', { index, message: event });

  handlingIds.splice(indexId, 1);
}

export function setUpListeners(ctx) {
  feathersClient.service('member-messages').on('patched', (event) => onMemberMessagesPatched(ctx, event));
}

export async function chats(ctx, data) {
  const connectionId = data.query.connectionId;
  // let connection = ctx.getters('member-connections/get', [connectionId], { root: true });
  // console.log
  const res = await feathersClient.service('member-message-direct').find(data);

  const list = [];
  let messagesPrepared = [];

  res.chats
    .filter((item) => ['status', 'broadcast'].some((remove) => !item.jid.includes(remove)))
    .forEach((item) => {
      messagesPrepared = [...messagesPrepared, ...item.messages.map((e) => ({
        ...e,
        messageTimestamp: Number(e.messageTimestamp),
        connectionId,
        jid: item.jid,
      }))];
      delete item.messages;
      item.connectionId = connectionId;
      item.modify_tag = item.modify_tag ? Number(item.modify_tag) : undefined;
      item.pin = Number(item.pin) || 0;
      list.push(item);
    });

  // messagesPrepared = messagesPrepared
  //   .sort((a, b) => a.messageTimestamp - b.messageTimestamp);

  ctx.commit('chatsMerge', { chats: list, cursor: res.cursor });
  ctx.commit('messages', { messages: messagesPrepared });
}

export async function messages(ctx, {
  jid, amount = 25, cursor = null, filter = null, connectionId,
}) {
  let parameters = [jid, amount];
  let action = 'loadMessages';

  if (cursor) {
    parameters.push(`parse:${JSON.stringify(cursor)}`);
  }

  if (filter) {
    action = 'searchMessages';
    parameters = [filter, jid, amount, 1];
  }

  const res = await feathersClient
    .service('member-message-direct')
    .find({
      query: {
        connectionId,
        action,
        parameters,
      },
    });

  const messagesFiltered = res.messages
    .filter((message) => !ctx.getters.messages(jid).some((e) => e.key.id === message.key.id))
    .map((item) => ({
      ...item,
      messageTimestamp: Number(item.messageTimestamp),
      connectionId,
      jid,
    }));

  if (messagesFiltered.length) {
    ctx.commit('messagesMerge', messagesFiltered);
  }

  return res;
}

export async function chat(ctx, { connectionId, jid }) {
  if (!jid) return;
  if (!connectionId) return;

  const res = await feathersClient
    .service('member-message-direct')
    .find({
      query: {
        connectionId,
        actionPath: 'chats',
        action: 'get',
        parameters: [jid],
      },
    });

  const messagesFiltered = res.messages
    .filter((message) => !ctx.getters.messages(jid).some((e) => e.key.id === message.key.id))
    .map((item) => ({
      ...item,
      messageTimestamp: Number(item.messageTimestamp),
      connectionId,
      jid,
    }));

  if (messagesFiltered.length) {
    ctx.commit('messagesMerge', messagesFiltered);
  }

  if (!ctx.getters.chat(jid)) {
    // eslint-disable-next-line no-shadow
    const chat = {
      ...res,
      connectionId,
    };
    delete chat.messages;
    chat.modify_tag = Number(chat.modify_tag);
    chat.pin = Number(chat.pin) || 0;
    ctx.commit('chatPush', chat);
  }
}

export async function markRead(ctx, {
  jid, connectionId,
}) {
  const parameters = [jid];
  const action = 'chatRead';

  const res = await feathersClient
    .service('member-message-direct')
    .find({
      query: {
        connectionId,
        action,
        parameters,
      },
    });

  ctx.commit('chatSetCount', { jid, count: 0 });

  return res;
}
