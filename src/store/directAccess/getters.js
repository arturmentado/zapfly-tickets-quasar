export function lastMessage(state, getter) {
  return function (jid) {
    if (!jid) return null;
    const list = getter.messages(jid);
    if (!list.length) return null;
    return list[list.length - 1];
  };
}

export function messages(state) {
  return function (jid) {
    if (!jid) return null;
    return state.messages.list
      .filter((e) => e.jid === jid);
  };
}

export function chat(state) {
  return function (jid) {
    if (!jid) return null;
    return state.chats.list
      .find((e) => e.jid === jid);
  };
}

export function chats(state) {
  return [...state.chats.list]
    .sort((a, b) => {
      if (a.pin < b.pin) return 1;
      if (a.pin > b.pin) return -1;

      if (a.t < b.t) return 1;
      if (a.t > b.t) return -1;

      return 0;
    });
}
