export function chats(state, data) {
  state.chats.list = data.chats;
  state.chats.cursor = data.cursor;
}

export function chatsMerge(state, data) {
  let chatsNew = state.chats.list.slice();
  chatsNew = [...chatsNew, ...data.chats.slice()];

  // remove duplicates
  chatsNew = chatsNew
    .filter(
      (item, index, self) => index === self
        .findIndex(
          (t) => (t.jid === item.jid && t.connectionId === item.connectionId),
        ),
    );

  state.chats.list = chatsNew;
  // state.chats.cursor = data.cursor;
}

export function messages(state, data) {
  state.messages.list = data.messages;
}

export function messageUpdateIndex(state, data) {
  state.messages.list[data.index] = data.message;
}

export function messagePush(state, data) {
  state.messages.list.push(data);
}

export function messagesMerge(state, data) {
  const list = [...state.messages.list, ...data].sort((a, b) => a.messageTimestamp - b.messageTimestamp);
  state.messages.list = list;
}

export function chatPush(state, data) {
  state.chats.list.push(data);
}

export function chatSetCount(state, data) {
  if (!data.jid) return;
  const index = state.chats.list.findIndex((e) => e.jid === data.jid);
  if (index < 0) return;
  state.chats.list[index].count = data.count;
}

export function chatSetT(state, data) {
  if (!data.jid) return;
  const index = state.chats.list.findIndex((e) => e.jid === data.jid);
  if (index < 0) return;
  state.chats.list[index].t = data.t;
}
