const routes = [
  {
    path: '/',
    component: () => import('layouts/Frameless.vue'),
    children: [
      {
        path: '',
        name: 'public',
        component: () => import('pages/public/Login.vue'),
      },
      {
        path: 'registrar',
        name: 'register',
        component: () => import('pages/public/Register.vue'),
      },
      {
        path: 'password-reset',
        name: 'passwordReset',
        component: () => import('pages/public/PasswordReset.vue'),
      },
      {
        path: 'termos-de-uso',
        name: 'terms',
        component: () => import('pages/public/Terms.vue'),
      },
      {
        path: 'politica-de-privacidade',
        name: 'privacy',
        component: () => import('pages/public/Privacy.vue'),
      },
    ],
  },

  {
    path: '/dashboard',
    component: () => import('layouts/Logged.vue'),
    children: [
      {
        path: '',
        name: 'dashboard',
        component: () => import('pages/dashboard/Dashboard.vue'),
      },
      {
        path: 's',
        name: 'settings',
        component: () => import('pages/dashboard/settings/Settings.vue'),
      },
      {
        path: 'catalogo',
        name: 'catalog',
        component: () => import('pages/dashboard/catalog/Catalog.vue'),
      },
      {
        path: 'c',
        name: 'chatbot',
        component: () => import('pages/dashboard/chatbot/Chatbot.vue'),
      },
      {
        path: 'a',
        name: 'tickets',
        component: () => import('pages/dashboard/ticket/Ticket.vue'),
        children: [
          {
            path: ':id',
            name: 'tickets-room',
            component: () => import('pages/dashboard/ticket/Room.vue'),
          },
        ],
      },
      {
        path: 'chats',
        name: 'chats',
        component: () => import('pages/dashboard/ticket/Chat.vue'),
        children: [
          {
            path: ':id/:connectionId',
            props: true,
            name: 'chats-room',
            component: () => import('pages/dashboard/ticket/ChatRoom.vue'),
          },
        ],
      },
      {
        path: 'api-desenvolvedor',
        name: 'api',
        component: () => import('pages/dashboard/api/Api.vue'),
      },
      {
        path: 'em-espera',
        name: 'tickets-on-hold',
        component: () => import('pages/dashboard/ticket/Ticket.vue'),
        props: {
          onHoldView: true,
        },
        children: [
          {
            path: ':id',
            name: 'tickets-room-on-hold',
            component: () => import('pages/dashboard/ticket/Room.vue'),
          },
        ],
      },
      {
        path: 'adm',
        component: () => import('layouts/Frameless'),
        children: [
          {
            path: 'usuarios',
            name: 'adm-users',
            component: () => import('pages/dashboard/adm/Users.vue'),
          },
          {
            path: 'conexoes',
            name: 'adm-connections',
            component: () => import('pages/dashboard/adm/Connections.vue'),
          },
          {
            path: 'avisos-e-ajudas',
            name: 'adm-tooltips',
            component: () => import('pages/dashboard/adm/Tooltip.vue'),
          },
          {
            path: 'ambiente',
            name: 'adm-env',
            component: () => import('pages/dashboard/adm/Env.vue'),
          },
        ],
      },
    ],
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue'),
  },
];

export default routes;
