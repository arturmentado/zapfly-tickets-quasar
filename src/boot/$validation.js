/* eslint-disable no-useless-escape */
import { scroll } from 'quasar';

// const emailRegex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
// eslint-disable-next-line no-useless-escape
const emailRegex = new RegExp('[a-z0-9!#$%&\'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&\'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?');
// eslint-disable-next-line no-control-regex
const urlRegex = new RegExp('[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)');
const { getScrollTarget, setScrollPosition } = scroll;

import Vue from 'vue';

export default async () => {
  Vue.mixin({
    data() {
      return {
        $rules: {
          required: (val) => (val !== null && val !== '') || this.$t('validations.neededField'),
          requiredIf: (cond) => (val) => {
            if (!cond || (cond && !!val)) {
              return true;
            }
            return this.$t('validations.neededField');
          },
          equals: (value, field) => (val) => value === val || this.$t('validations.equals', { field }),
          lesserThan: (value) => (val) => parseFloat(val) < value || this.$t('validations.lesserThan', { value }),
          lesserThanOrEqual: (value) => (val) => parseFloat(val) <= value || this.$t('validations.lesserThanOrEqual', { value }),
          greaterThan: (value) => (val) => parseFloat(val) > 0 || this.$t('validations.greaterThan', { value }),
          greaterThanOrEqual: (value) => (val) => parseFloat(val) >= value || this.$t('validations.greaterThanOrEqual', { value }),
          minLength: (value) => (val) => !val || val.length >= value || this.$t('validations.minLength', { value }),
          email: (val) => !val || emailRegex.test(val) || this.$t('validations.email'),
          url: (val) => !val || urlRegex.test(val) || this.$t('validations.url'),
        },
      };
    },
    methods: {
      async $validate(form) {
        if (!form) return true;
        let valid = true;
        let el;
        for await (const key of Object.keys(form)) {
          if (Array.isArray(form[key])) {
            for (let i = 0; i < form[key].length; i += 1) {
              for await (const subKey of Object.keys(form[key][i])) {
                valid = await this.$refs[`${key}${i}_${subKey}`][0].validate() && valid;
                if (!valid && (!el || el.$el.offsetTop > this.$refs[`${key}${i}_${subKey}`][0].$el.offsetTop)) {
                  el = this.$refs[`${key}${i}_${subKey}`][0];
                }
              }
            }
          } else if (this.$refs[key]) {
            // Form components
            if (this.$refs[key].onValidate) {
              valid = await this.$refs[key].onValidate() && valid;
            } else if (this.$refs[key].validate) {
              valid = await this.$refs[key].validate() && valid;
            }
            if (!valid && (!el || el.$el.offsetTop > this.$refs[key].$el.offsetTop)) {
              el = this.$refs[key];
            }
          }
        }
        if (!valid) {
          setScrollPosition(getScrollTarget(el.$el), el.$el.offsetTop, 300);
        }
        return valid;
      },
      async $resetValidation(form) {
        await this.$nextTick();
        if (!form) return;
        for (const key of Object.keys(form)) {
          if (Array.isArray(form[key])) {
            for (let i = 0; i < form[key].length; i += 1) {
              for (const subKey of Object.keys(form[key][i])) {
                if (this.$refs[`${key}${i}_${subKey}`][0] && this.$refs[`${key}${i}_${subKey}`][0].resetValidation) {
                  this.$refs[`${key}${i}_${subKey}`][0].resetValidation();
                }
              }
            }
          } else if (this.$refs[key] && this.$refs[key].resetValidation) {
            this.$refs[key].resetValidation();
          }
        }
      },
    },
  });
};
