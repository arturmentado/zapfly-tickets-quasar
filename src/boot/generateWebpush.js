const VAPID_PUBLIC_KEY = 'BNpoC1stb7idehmljDDJgJh8k3cHhVwSq-S6QVEPBFMRo_vsBZ8aMb-VU8_PmavFdOTRS5wCfmTkUCtCqTmWOPQ';

function urlB64ToUint8Array(base64String) {
  // eslint-disable-next-line no-mixed-operators
  const padding = '='.repeat((4 - base64String.length % 4) % 4);
  const base64 = (base64String + padding)
    // eslint-disable-next-line no-useless-escape
    .replace(/\-/g, '+')
    .replace(/_/g, '/');

  const rawData = window.atob(base64);
  const outputArray = new Uint8Array(rawData.length);

  for (let i = 0; i < rawData.length; i += 1) {
    outputArray[i] = rawData.charCodeAt(i);
  }
  return outputArray;
}

export default function (registration) {
  const subscribeOptions = {
    userVisibleOnly: true,
    applicationServerKey: urlB64ToUint8Array(VAPID_PUBLIC_KEY),
  };

  return registration.pushManager.subscribe(subscribeOptions);
}
