function doFormat(format, date) {
  const intl = new Intl.DateTimeFormat('pt-BR', format);

  return intl.format(date);
}

function jsDateFormat(dateString) {
  if (!dateString) return;
  const format = {
    year: 'numeric',
    month: 'numeric',
    day: 'numeric',
    hour: 'numeric',
    minute: 'numeric',
  };

  dateString = new Date(dateString);
  if (!(dateString instanceof Date)) return;
  return doFormat(format, dateString);
}

function jsDateFormatMin(dateString) {
  if (!dateString) return;
  const format = {
    year: 'numeric',
    month: 'numeric',
    day: 'numeric',
  };

  dateString = new Date(dateString);
  if (!(dateString instanceof Date)) return;
  return doFormat(format, dateString);
}

function timestampFormat(dateString) {
  if (!dateString) return;
  const format = {
    year: 'numeric',
    month: 'numeric',
    day: 'numeric',
    hour: 'numeric',
    minute: 'numeric',
  };

  dateString = new Date(dateString);
  if (!(dateString instanceof Date)) return;
  return doFormat(format, dateString);
}

function mmssFormat(dateString) {
  if (!dateString) return;
  const format = {
    minute: 'numeric',
    second: 'numeric',
  };

  dateString = new Date(dateString);
  if (!(dateString instanceof Date)) return;
  return doFormat(format, dateString);
}

function fromNow(date) {
  const seconds = Math.floor((new Date() - date) / 1000);
  const years = Math.floor(seconds / 31536000);
  const months = Math.floor(seconds / 2592000);
  const days = Math.floor(seconds / 86400);

  if (days > 548) {
    return `${years}${this.$t('date.manyYears')}`;
  }
  if (days >= 320 && days <= 547) {
    return `${this.$t('date.aYearAgo')}`;
  }
  if (days >= 45 && days <= 319) {
    return `${months} ${this.$t('date.monthsAgo')}`;
  }
  if (days >= 26 && days <= 45) {
    return `${this.$t('date.aMonthAgo')}`;
  }

  const hours = Math.floor(seconds / 3600);

  if (hours <= -36 && days >= -25) {
    return `${days * -1} ${this.$t('date.days')}`;
  }

  if (hours >= 36 && days <= 25) {
    return `${days} ${this.$t('date.daysAgo')}`;
  }
  if (hours >= 22 && hours <= 35) {
    return `${this.$t('date.aDayAgo')}`;
  }

  const minutes = Math.floor(seconds / 60);

  if (minutes >= 90 && hours <= 21) {
    return `${hours} ${this.$t('date.hoursAgo')}`;
  }
  if (minutes >= 45 && minutes <= 89) {
    return `${this.$t('date.aHourAgo')}`;
  }
  if (seconds >= 90 && minutes <= 44) {
    return `${minutes} ${this.$t('date.minutesAgo')}`;
  }
  if (seconds >= 45 && seconds <= 89) {
    return `${this.$t('date.aMinuteAgo')}`;
  }
  if (seconds >= 0 && seconds <= 45) {
    return `${this.$t('date.fewSeconds')}`;
  }
}

export {
  jsDateFormat, jsDateFormatMin, mmssFormat, fromNow,
};

export default ({ Vue }) => {
  Vue.prototype.$jsDateFormat = jsDateFormat;
  Vue.prototype.$jsDateFormatMin = jsDateFormatMin;
  Vue.prototype.$timestampFormat = timestampFormat;
  Vue.prototype.$mmssFormat = mmssFormat;
  Vue.prototype.$fromNow = fromNow;
};
