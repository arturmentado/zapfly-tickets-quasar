import localforage from 'localforage';

const lf1 = localforage.createInstance({
  name: 'zapfly_tickets',
});

export default ({ Vue, store }) => {
  Vue.prototype.$lf = lf1;
  store.$lf = lf1;
};

export const lf = lf1;
