import { Notify } from 'quasar';
import { i18n } from './i18n';

const notify = (data, message = '', typeOveride = null, params = null) => {
  const options = {
    positive: {
      color: 'positive',
      icon: 'check',
      message: i18n.t('commonWord.success'),
    },
    negative: {
      color: 'negative',
      icon: 'close',
      message: i18n.t('notifications.negative'),
    },
    warning: {
      color: 'orange-8',
      icon: 'warning',
    },
    info: {
      color: 'info',
      icon: 'info',
    },
  };

  let configObject = {
    message: '',
    timeout: 2600,
    position: 'bottom-right',
    color: 'negative',
    icon: 'fas fa-times',
  };

  if (typeof data === 'string') typeOveride = data;

  if (message) configObject.message = message;

  if (typeOveride) {
    configObject.color = options[typeOveride].color;
    configObject.icon = options[typeOveride].icon;
    configObject.message = message || options[typeOveride].message;
  }

  if (params) configObject = params;

  Notify.create(configObject);
};

export { notify };

export default ({ Vue }) => {
  Vue.prototype.$notify = notify;
};
