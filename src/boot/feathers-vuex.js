// src/feathers-client.js
import feathers from '@feathersjs/feathers';
import socketio from '@feathersjs/socketio-client';
import auth from '@feathersjs/authentication-client';
import io from 'socket.io-client';
import { iff, discard } from 'feathers-hooks-common';
import feathersVuex from 'feathers-vuex';
import { Dialog, Screen } from 'quasar';
import { i18n } from './i18n';

const url = process.env.API_URL ?? 'zapfly-be.arcode.online';
// const socket = io('wss://zapfly-be.arcode.online', { transports: ['websocket'], upgrade: false });
const socket = io(url, { transports: ['websocket'], upgrade: false });

const feathersClient = feathers()
  .configure(socketio(socket))
  .configure(auth({ storageKey: 'auth', storage: window.localStorage }))
  .hooks({
    before: {
      all: [
        iff(
          (context) => ['create', 'update', 'patch'].includes(context.method),
          discard('__id', '__isTemp'),
        ),
      ],
    },
    error: {
      all: [
        (ctx) => {
          if (ctx.error.name === 'NotAuthenticated' && window.location.href.includes('/dashboard')) {
            window.location.reload();
          }
        },
      ],
    },
  });

let dialogNet;
// Check BE Version
socket.on('disconnect', () => {
  if (Screen.lt.sm) return;
  dialogNet = Dialog.create({
    message: i18n.t('connection.internetProblems'),
    progress: true, // we enable default settings
    persistent: true, // we want the user to not be able to close it
    ok: false, // we want the user to not be able to close it
  });
});

socket.on('connect', () => {
  if (dialogNet) {
    dialogNet.update({
      message: i18n.t('connection.back'),
    });

    setTimeout(() => {
      dialogNet.hide();
    }, 300);
  }
});

export default feathersClient;

// Setting up feathers-vuex
const {
  makeServicePlugin, makeAuthPlugin, BaseModel, models, FeathersVuex,
} = feathersVuex(
  feathersClient,
  {
    // serverAlias: 'api', // optional for working with multiple APIs (this is the default value)
    idField: '_id', // Must match the id field in your database table/collection
    whitelist: ['$populate', '$search', '$regex', '$options'],
  },
);

export {
  makeAuthPlugin, makeServicePlugin, BaseModel, models, FeathersVuex, socket,
};
