import Tracker from '@openreplay/tracker';
import trackerAssist from '@openreplay/tracker-assist';
import Vue from 'vue';

const tracker = new Tracker({
  projectKey: 'naLVNxRdZQdOqlwDM6HZ',
  ingestPoint: 'https://replay.arcode.online/ingest',
});
tracker.use(trackerAssist({})); // check the list of available options below

tracker.start();
export default () => {
  console.log('open replay working');
};
Vue.prototype.$tracker = tracker;
