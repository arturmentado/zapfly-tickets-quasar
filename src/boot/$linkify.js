export default ({ Vue }) => {
  Vue.prototype.$copy = function (toCopy, notify = true) {
    const el = document.createElement('textarea');
    el.value = toCopy;
    document.body.appendChild(el);
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);

    if (notify) this.$notify('positive', this.$t('linkify.copied'));
  };

  Vue.prototype.$removeLinkify = function (inputText) {
    if (!inputText) return inputText;

    let replacedText = inputText;
    // eslint-disable-next-line no-useless-escape
    const replacePattern4 = /\*(.*?)\*/gm;
    replacedText = replacedText.replace(replacePattern4, '$1');

    // eslint-disable-next-line no-useless-escape
    const replacePattern5 = /\~(.*?)\~/gm;
    replacedText = replacedText.replace(replacePattern5, '$1');

    // eslint-disable-next-line no-useless-escape
    const replacePattern6 = /\_(.*?)\_/gm;
    replacedText = replacedText.replace(replacePattern6, '$1');

    // eslint-disable-next-line no-useless-escape
    const replacePattern7 = /\`\`\`(.*?)\`\`\`/gm;
    replacedText = replacedText.replace(replacePattern7, '$1');

    return replacedText;
  };

  Vue.prototype.$linkifyBasic = function (inputText) {
    if (!inputText) return inputText;

    return inputText;
  };

  Vue.prototype.$linkify = function (inputText, signature = null, append = false) {
    if (!inputText) return inputText;

    let replacedText;

    replacedText = this.$linkifyBasic(inputText);

    // eslint-disable-next-line no-useless-escape
    const replacePattern4 = /\*(.*?)\*/gm;
    replacedText = replacedText.replace(replacePattern4, '**$1**');

    if (signature) {
      signature = signature.split('<meu-nome>').join(this.$user.name);

      if (append) replacedText += `\n${signature}`;
      else replacedText = `${signature}\n${replacedText}`;
    }

    return replacedText;
  };
};
