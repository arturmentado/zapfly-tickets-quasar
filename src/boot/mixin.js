import Vue from 'vue';

export default async () => {
  Vue.mixin({

    computed: {
      $chatbotClosingActive() {
        return this.$store.getters['member-chatbots/find']({ query: { $limit: 0, type: 'closing', active: true } }).total;
      },
      $isAuth() {
        return this.$store.getters['auth/isAuthenticated'];
      },
      $env() {
        return this.$store.getters['envs/get']('zapfly');
      },
      $connections() {
        return this.$store.getters['member-connections/list'];
      },
      $user() {
        return this.$store.getters['auth/user'];
      },
      $users() {
        return this.$store.getters['members/list'].filter((e) => !e.ghost);
      },
      $isAdmin() {
        return this.$user.permissions.includes('admin');
      },
      $isManager() {
        return !this.$isAdmin && this.$user.permissions.includes('supervisor');
      },
      $isAttendant() {
        return !this.$isAdmin && !this.$isManager && this.$user.permissions.includes('attendant');
      },
      $isSystemAdmin() {
        if (!this.$store.getters['auth/isAuthenticated']) return false;
        return this.$user.permissions.includes('super-admin');
      },
    },
    methods: {
      async $reopenTicket(ticket) {
        const options = {
          select: ['jid', 'status'],
          query: { jid: ticket.jid, status: { $in: ['waiting', 'in_progress'] } },
        };
        const haveOneOpen = await this.$store.dispatch('member-tickets/find', options);
        if (haveOneOpen.total) {
          this.$notify('negative', this.$t('ticket.alreadyOpen'));
          return;
        }

        if (
          this.$store.getters['member-settings/list'].some((e) => e.name === 'allowFastReopen' && e.value === true)
          || ticket.status === 'closing'
        ) {
          this.$router.push({ name: 'tickets-room', params: { id: ticket._id } });
          if (ticket.attendantId) {
            this.patch([ticket._id, { status: 'in_progress' }]);
            return;
          }
          this.patch([ticket._id, { status: 'waiting' }]);
          return;
        }

        this.$q.dialog({
          title: this.$t('ticket.reopen'),
          message: this.$t('ticket.canCreateNew'),
          ok: {
            label: this.$t('ticket.createNewLabel'),
          },
          cancel: {
            flat: true,
            label: this.$t('ticket.reopenThis'),
          },
        }).onOk(async () => {
          const body = {
            connectionId: ticket.connectionId,
            contactId: ticket.contact._id,
            jid: ticket.jid,
            status: 'in_progress',
            attendantId: this.$user._id,
          };
          const result = await this.$store.dispatch('member-tickets/create', body);
          this.$router.push({ name: 'tickets-room', params: { id: result._id } });
        }).onCancel(() => {
          this.$router.push({ name: 'tickets-room', params: { id: ticket._id } });
          if (ticket.attendantId) {
            this.patch([ticket._id, { status: 'in_progress' }]);
            return;
          }
          this.patch([ticket._id, { status: 'waiting' }]);
        });
      },

      $pushTicketToPipeline(id) {
        const ticket = this.$store.getters['member-tickets/get'](id);
        const undoParams = ticket ? { attendantId: ticket.attendantId, status: ticket.status, count: ticket.count } : { status: 'waiting' };
        const status = 'waiting';
        return this.$store.dispatch('member-tickets/patch', [id, { status, count: 0 }])
          .then(() => {
            this.$q.notify({
              message: status === 'closing' ? this.$t('ticket.closing') : this.$t('ticket.closedTicket'),
              actions: [
                {
                  label: this.$t('commonWord.undo'),
                  color: 'white',
                  handler: () => {
                    this.$store.dispatch('member-tickets/patch', [id, undoParams]);
                  },
                },
              ],
            });
          });
      },

      $closeTicket(id, force = false) {
        const ticket = this.$store.getters['member-tickets/get'](id);
        const undoParams = ticket ? { attendantId: ticket.attendantId, status: ticket.status, count: ticket.count } : { status: 'waiting' };
        const status = this.$chatbotClosingActive && !force ? 'closing' : 'closed';
        return this.$store.dispatch('member-tickets/patch', [id, { status, count: 0 }])
          .then(() => {
            this.$q.notify({
              message: status === 'closing' ? this.$t('ticket.closing') : this.$t('ticket.closedTicket'),
              actions: [
                {
                  label: this.$t('commonWord.undo'),
                  color: 'white',
                  handler: () => {
                    this.$store.dispatch('member-tickets/patch', [id, undoParams]);
                  },
                },
              ],
            });
          });
      },

      async $restoreOnHoldTicket(id, jid) {
        const check = await this.$store.dispatch('member-tickets/find', { query: { $limit: 0, jid, status: 'waiting' } });
        if (check.total) return this.$notify('negative', this.$t(''));

        const ticket = this.$store.getters['member-tickets/get'](id);
        const undoParams = ticket ? { attendantId: ticket.attendantId, status: ticket.status } : { status: 'waiting' };
        return this.$store.dispatch('member-tickets/patch', [id, { status: 'waiting', reason: null }])
          .then(() => {
            this.$q.notify({
              message: this.$t('ticket.restore'),
              actions: [
                {
                  label: this.$t('commonWord.undo'),
                  color: 'white',
                  handler: () => {
                    this.$store.dispatch('member-tickets/patch', [id, undoParams]);
                  },
                },
              ],
            });
          });
      },

      $putOnHoldTicket(id) {
        const ticket = this.$store.getters['member-tickets/get'](id);
        const undoParams = ticket ? { attendantId: ticket.attendantId, status: ticket.status } : { status: 'waiting' };
        this.$q.dialog({
          title: this.$t('commonWord.reason'),
          message: this.$t('ticket.waitingReason'),
          prompt: {
            model: this.$t('commonWord.others'),
          },
          cancel: {
            label: this.$t('commonWord.cancel'),
          },
          ok: {
            label: this.$t('commonWord.send'),
          },
        }).onOk((e) => {
          this.$store.dispatch('member-tickets/patch', [id, { status: 'on_hold', reason: e }])
            .then(() => {
              this.$q.notify({
                message: this.$t('ticket.onHold'),
                actions: [
                  {
                    label: this.$t('commonWord.undo'),
                    color: 'white',
                    handler: () => {
                      this.$store.dispatch('member-tickets/patch', [id, undoParams]);
                    },
                  },
                ],
              });
            });
        });
      },

      $isPaymentOk(conn) {
        if (!conn.preapprovalPlan) return false;
        const { status, createdAt, extraTrial } = conn.preapprovalPlan;

        if (extraTrial) {
          const extraTrialTimestamp = new Date(extraTrial).getTime();
          if (new Date().getTime() < extraTrialTimestamp) return true;
        }

        const timestampFrom7Days = new Date().getTime() - (7 * 24 * 60 * 60 * 1000);
        const createdAtTimestamp = new Date(createdAt).getTime();

        if (status === 'TRIAL' && createdAtTimestamp > timestampFrom7Days) return true;

        const validStatus = ['created', 'ACTIVE'];
        if (!validStatus.includes(status)) return false;

        return true;
      },
    },
  });
};
