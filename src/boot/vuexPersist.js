// import VuexPersistence from 'vuex-persist';
import localforage from 'localforage';

const lf1 = localforage.createInstance({
  name: 'zapfly_tickets_vuex',
});

export const LF = lf1;
// export default ({ store }) => {
//   new VuexPersistence({
//     storage: lf1,
//     asyncStorage: true,
//   }).plugin(store);
// };
